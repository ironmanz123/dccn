﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Edu_Hutech.Models;

namespace Edu_Hutech.Controllers
{
    public class TimKiemController : Controller
    {
        HutechEduDataContext data = new HutechEduDataContext();
        // GET: TimKiem
        [HttpPost]
        public ActionResult KetQuaTimKiem(FormCollection f)
        {
            string sTuKhoa = f["txtTimKiem"].ToString();
            string mssv = data.SinhViens.Where(s => s.MaLop == sTuKhoa && (s.loptruong == true) ).Select(s => s.MSSV).FirstOrDefault();
            var a = data.DSach_DK_LHTT().Where(n => n.MSSV == mssv).ToList();
            return View(a);
        }
       
    }
}