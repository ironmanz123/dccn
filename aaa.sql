USE [master]
GO
/****** Object:  Database [DACN]    Script Date: 29/12/2018 03:00:32 CH ******/
CREATE DATABASE [DACN]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DACN', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.BINKAYEO\MSSQL\DATA\DACN.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DACN_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.BINKAYEO\MSSQL\DATA\DACN_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [DACN] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DACN].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DACN] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DACN] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DACN] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DACN] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DACN] SET ARITHABORT OFF 
GO
ALTER DATABASE [DACN] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DACN] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DACN] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DACN] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DACN] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DACN] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DACN] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DACN] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DACN] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DACN] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DACN] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DACN] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DACN] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DACN] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DACN] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DACN] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DACN] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DACN] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DACN] SET  MULTI_USER 
GO
ALTER DATABASE [DACN] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DACN] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DACN] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DACN] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DACN] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DACN] SET QUERY_STORE = OFF
GO
USE [DACN]
GO
/****** Object:  Table [dbo].[CanBo_HuongDan]    Script Date: 29/12/2018 03:00:32 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CanBo_HuongDan](
	[MaGV] [varchar](8) NOT NULL,
	[TenGV] [nvarchar](50) NOT NULL,
	[SDT] [bigint] NULL,
	[DiaChi] [nvarchar](100) NULL,
	[Email] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](30) NULL,
	[pass] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CapDanhHieu]    Script Date: 29/12/2018 03:00:32 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CapDanhHieu](
	[MaCap] [varchar](2) NOT NULL,
	[TenCap] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaCap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeTai_NCKH]    Script Date: 29/12/2018 03:00:32 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeTai_NCKH](
	[MSSV] [char](10) NOT NULL,
	[MaPDK_NCKH] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MSSV] ASC,
	[MaPDK_NCKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Khoa]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Khoa](
	[MaKhoa] [varchar](5) NOT NULL,
	[TenKhoa] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaKhoa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KQ_danhgia_LHTT_SV]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KQ_danhgia_LHTT_SV](
	[MaPDK_LHTT] [int] NOT NULL,
	[Ma_NDTieuChi] [varchar](10) NOT NULL,
	[TinhTrang] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPDK_LHTT] ASC,
	[Ma_NDTieuChi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KQ_danhgia_SV5T_SV]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KQ_danhgia_SV5T_SV](
	[Ma_NDTieuChi] [varchar](10) NOT NULL,
	[MaPDK_SV5T] [bigint] NOT NULL,
	[TinhTrang] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Ma_NDTieuChi] ASC,
	[MaPDK_SV5T] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KQ_DanhGiaND_LHTT_Admin]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KQ_DanhGiaND_LHTT_Admin](
	[TaiKhoan] [varchar](20) NOT NULL,
	[MaPDK_LHTT] [int] NOT NULL,
	[Ma_NDTieuChi] [varchar](10) NOT NULL,
	[Diem] [int] NULL,
	[GhiChu] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[TaiKhoan] ASC,
	[MaPDK_LHTT] ASC,
	[Ma_NDTieuChi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KQ_DanhGiaND_SV5T_Admin]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KQ_DanhGiaND_SV5T_Admin](
	[TaiKhoan] [varchar](20) NOT NULL,
	[Ma_NDTieuChi] [varchar](10) NOT NULL,
	[MaPDK_SV5T] [bigint] NOT NULL,
	[Dat] [bit] NULL,
	[GhiChu] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[TaiKhoan] ASC,
	[Ma_NDTieuChi] ASC,
	[MaPDK_SV5T] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KQ_DeTai_NCKH]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KQ_DeTai_NCKH](
	[MaPDK_NCKH] [bigint] NOT NULL,
	[MaCap] [varchar](2) NOT NULL,
	[KQ] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPDK_NCKH] ASC,
	[MaCap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lop]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lop](
	[MaLop] [varchar](10) NOT NULL,
	[MaKhoa] [varchar](5) NOT NULL,
	[TenLop] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MinhChung]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MinhChung](
	[MaMinhChung] [bigint] IDENTITY(1,1) NOT NULL,
	[TenMInhChung] [nvarchar](20) NULL,
	[URL] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaMinhChung] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PDK_BaiBao_ThamLuan_NCKH]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PDK_BaiBao_ThamLuan_NCKH](
	[MaPDK_BBao_TL_CKH] [bigint] IDENTITY(1,1) NOT NULL,
	[TenBaiBao] [nvarchar](30) NULL,
	[NoiDung] [nvarchar](50) NULL,
	[MSSV] [char](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPDK_BBao_TL_CKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhanCap_TieuChi]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhanCap_TieuChi](
	[MaPhanCap] [varchar](8) NOT NULL,
	[Khoa] [bit] NULL,
	[Truong] [bit] NULL,
	[Thanh] [bit] NULL,
	[TrungUong] [bit] NULL,
	[PhanLoai] [varchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPhanCap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhanLoai]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhanLoai](
	[PhanLoai] [varchar](2) NOT NULL,
	[BatBuoc] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PhanLoai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhieuDangKy_DeTai_NCKH]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuDangKy_DeTai_NCKH](
	[MaPDK_NCKH] [bigint] IDENTITY(1,1) NOT NULL,
	[Ma_GVHD1] [varchar](8) NOT NULL,
	[Ma_GVHD2] [varchar](8) NULL,
	[TenDeTai] [nvarchar](100) NOT NULL,
	[NgayDK] [datetime] NULL,
	[ND_DeTai] [nvarchar](200) NULL,
	[TinhTrang] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPDK_NCKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhieuDangKy_LHTT]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuDangKy_LHTT](
	[MaPDK_LHTT] [int] IDENTITY(1,1) NOT NULL,
	[MaMinhChung] [bigint] NOT NULL,
	[NamHoc] [int] NULL,
	[MSSV] [char](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPDK_LHTT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhieuDangKy_SV5T]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuDangKy_SV5T](
	[MaPDK_SV5T] [bigint] IDENTITY(1,1) NOT NULL,
	[MaMinhChung] [bigint] NOT NULL,
	[MaCap] [varchar](2) NOT NULL,
	[NamHoc] [int] NULL,
	[MSSV] [char](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPDK_SV5T] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhongBan]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhongBan](
	[MaPhongBan] [varchar](10) NULL,
	[TenPhongBan] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SinhVien]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SinhVien](
	[MSSV] [char](10) NOT NULL,
	[MaLop] [varchar](10) NOT NULL,
	[Ho] [nvarchar](20) NOT NULL,
	[Ten] [nvarchar](10) NOT NULL,
	[NgaySinh] [datetime] NOT NULL,
	[SDT] [bigint] NULL,
	[QueQuan] [nvarchar](50) NULL,
	[GioiTinh] [bit] NULL,
	[password] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[NoiSinh] [nvarchar](50) NULL,
	[DiaChiCuTru] [nvarchar](150) NULL,
	[DiaChiLienLac] [nvarchar](150) NULL,
	[NgayKetNapDoan] [datetime] NULL,
	[NgayVaoDang_dubi_neuco] [datetime] NULL,
	[NgayVaoDang_chinhthuc_neuco] [datetime] NULL,
	[ChucVuDoan_neuco] [nvarchar](50) NULL,
	[loptruong] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MSSV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaiKhoan_ADMIN]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan_ADMIN](
	[TaiKhoan] [varchar](20) NOT NULL,
	[MatKhau] [nvarchar](20) NOT NULL,
	[PhanCap] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThamSo]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThamSo](
	[Key] [int] IDENTITY(1,1) NOT NULL,
	[ThuocTinh] [nvarchar](10) NULL,
	[GiaTri] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThoiGianHD]    Script Date: 29/12/2018 03:00:33 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThoiGianHD](
	[TenHD] [varchar](5) NOT NULL,
	[TG_BatDau] [datetime] NULL,
	[TG_KetThuc] [datetime] NULL,
	[GhiChu] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[TenHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TieuChi]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TieuChi](
	[Ma_NDTieuChi] [varchar](10) NOT NULL,
	[MaTieuChi] [varchar](10) NOT NULL,
	[TenNDTieuChi] [nvarchar](200) NOT NULL,
	[Diem] [float] NULL,
	[MaPhanCap] [varchar](8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Ma_NDTieuChi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TieuChuan]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TieuChuan](
	[MaTieuChi] [varchar](10) NOT NULL,
	[TenTieuChi] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaTieuChi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TieuChuan_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TieuChuan_LHTT](
	[MaCap] [varchar](2) NOT NULL,
	[DIem] [int] NULL,
	[Ma_TC] [varchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Ma_TC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[CanBo_HuongDan] ([MaGV], [TenGV], [SDT], [DiaChi], [Email], [GhiChu], [pass]) VALUES (N'CH01', N'Trương Quốc Cường', 84901279062, N'', N'', N'', N'CH01')
INSERT [dbo].[CanBo_HuongDan] ([MaGV], [TenGV], [SDT], [DiaChi], [Email], [GhiChu], [pass]) VALUES (N'TG01', N'Văn Tuấn', 84901279000, N'', N'', N'', N'TG01')
INSERT [dbo].[CanBo_HuongDan] ([MaGV], [TenGV], [SDT], [DiaChi], [Email], [GhiChu], [pass]) VALUES (N'TG02', N'Trương Minh', 84901279062, N'', N'', N'', N'TG02')
INSERT [dbo].[CapDanhHieu] ([MaCap], [TenCap]) VALUES (N'K', N'Cấp Khoa')
INSERT [dbo].[CapDanhHieu] ([MaCap], [TenCap]) VALUES (N'TH', N'Cấp Thành')
INSERT [dbo].[CapDanhHieu] ([MaCap], [TenCap]) VALUES (N'TR', N'Cấp Trường')
INSERT [dbo].[CapDanhHieu] ([MaCap], [TenCap]) VALUES (N'TW', N'Cấp Trung Ương')
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511061266', 7)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511061266', 8)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065001', 14)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065001', 15)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065001', 16)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065001', 17)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065002', 11)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065002', 12)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065002', 13)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065005', 18)
INSERT [dbo].[DeTai_NCKH] ([MSSV], [MaPDK_NCKH]) VALUES (N'1511065151', 18)
INSERT [dbo].[Khoa] ([MaKhoa], [TenKhoa]) VALUES (N'CNTP', N'Công Nghệ Thực Phẩm')
INSERT [dbo].[Khoa] ([MaKhoa], [TenKhoa]) VALUES (N'CNTT', N'Công Nghệ Thông Tin')
INSERT [dbo].[Khoa] ([MaKhoa], [TenKhoa]) VALUES (N'KT', N'Tài Chính Ngân Hàng')
INSERT [dbo].[Khoa] ([MaKhoa], [TenKhoa]) VALUES (N'MT', N'Mỹ Thuật')
INSERT [dbo].[Khoa] ([MaKhoa], [TenKhoa]) VALUES (N'OTO', N'Cơ Điện')
INSERT [dbo].[Khoa] ([MaKhoa], [TenKhoa]) VALUES (N'QTKD', N'Quản Trị Kinh Doanh')
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'HT1', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'HT2', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'HT3', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'HT4', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'HT5', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'KYNANG1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'KYNANG2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'RL1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'RL2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'RL3', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'RL4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (4, N'RL5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'HT1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'HT2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'HT3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'HT4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'HT5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'KYNANG1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'KYNANG2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'RL1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'RL2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'RL3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'RL4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (5, N'RL5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'HT1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'HT2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'HT3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'HT4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'HT5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'KYNANG1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'KYNANG2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'RL1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'RL2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'RL3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'RL4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (7, N'RL5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'HT1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'HT2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'HT3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'HT4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'HT5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'KYNANG1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'KYNANG2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'RL1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'RL2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'RL3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'RL4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (8, N'RL5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'HT1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'HT2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'HT3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'HT4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'HT5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'KYNANG1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'KYNANG2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'RL1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'RL2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'RL3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'RL4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (10, N'RL5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'HT1', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'HT2', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'HT3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'HT4', 1)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'HT5', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'KYNANG1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'KYNANG2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'RL1', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'RL2', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'RL3', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'RL4', 0)
INSERT [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi], [TinhTrang]) VALUES (11, N'RL5', 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'B1', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'B1', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'B1', 10015, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'B1', 10017, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'BDKT', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'BDKT', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'BViet01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'BViet01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'BViet02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DANG', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DANG', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DGHT02', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DGHT02', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DGHT03', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DGHT04', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DGHT05', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DGHT05', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DHT01', 10008, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DHT01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DHT02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DRL', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DRL', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DRL', 10015, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DRL', 10017, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DVXS', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DVXS', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DVXS', 10015, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'DVXS', 10017, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTHT01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTHT01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTST', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTTV', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTTV', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTTV', 10015, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'GTTV', 10017, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDDC', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDDC', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDGLQT', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDGLQT', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDGLQT', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDHN01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDHN01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDHN02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN02', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN02', 10015, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN02', 10017, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN03', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HDTN04', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HMTN01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HMTN01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HMTN02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'HTMac', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'KN01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'KN01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'KN02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'KT01', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'NCKH01', 10008, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'NCKH01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'NCKH02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'NOIQUY', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'SVn1', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'SVn1', 10015, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'SVn1', 10017, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TLHT01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TLHT01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TLHT02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNKHOE01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNKHOE01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNKHOE02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNTT', 10009, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNTT', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNTT', 10015, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TNTT', 10017, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TVDT', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TVDT01', 10008, 1)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TVDT01', 10016, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'TVDT02', 10014, 0)
INSERT [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T], [TinhTrang]) VALUES (N'VHLS', 10014, 0)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'HT1', 12, N'asd')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'HT2', 0, N'noob')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'HT3', 5, N'dd')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'HT4', 15, N'ddd')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'HT5', 8, N'')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'KYNANG1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'KYNANG2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'RL1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'RL2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'RL3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'RL4', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'cntt01', 4, N'RL5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'HT1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'HT2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'HT3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'HT4', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'HT5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'KYNANG1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'KYNANG2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'RL1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'RL2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'RL3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'RL4', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 5, N'RL5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'HT1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'HT2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'HT3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'HT4', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'HT5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'KYNANG1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'KYNANG2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'RL1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'RL2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'RL3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'RL4', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 8, N'RL5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'HT1', 12, N'')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'HT2', 5, N'')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'HT3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'HT4', 15, N'')
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'HT5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'KYNANG1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'KYNANG2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'RL1', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'RL2', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'RL3', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'RL4', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_LHTT_Admin] ([TaiKhoan], [MaPDK_LHTT], [Ma_NDTieuChi], [Diem], [GhiChu]) VALUES (N'CNTT01', 11, N'RL5', 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'B1', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'B1', 10017, 1, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'BDKT', 10008, 1, N'aaa')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'BViet01', 10008, 0, N'sd')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DANG', 10008, 0, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DGHT02', 10008, 0, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DGHT05', 10008, 0, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DHT01', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DRL', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DRL', 10017, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DVXS', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'DVXS', 10017, 1, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'GTHT01', 10008, 0, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'GTTV', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'GTTV', 10017, 1, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HDDC', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HDGLQT', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HDHN01', 10008, 0, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HDTN01', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HDTN02', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HDTN02', 10017, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'HMTN01', 10008, 0, N'')
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'KN01', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'NCKH01', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'SVn1', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'SVn1', 10017, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'TLHT01', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'TNKHOE01', 10008, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'TNTT', 10009, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'TNTT', 10017, 0, NULL)
INSERT [dbo].[KQ_DanhGiaND_SV5T_Admin] ([TaiKhoan], [Ma_NDTieuChi], [MaPDK_SV5T], [Dat], [GhiChu]) VALUES (N'CNTT01', N'TVDT01', 10008, 0, NULL)
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DKT11', N'KT', N'')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH10', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH11', N'CNTT', N'')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH12', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH13', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH14', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH15', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTH20', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTO11', N'OTO', N'')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'15DTP01', N'CNTP', N'')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'16DTHB4', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'17DTHC2', N'CNTT', N' ')
INSERT [dbo].[Lop] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'18DTH02', N'CNTT', N' ')
SET IDENTITY_INSERT [dbo].[MinhChung] ON 

INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (1, N'1511061260', N'~/MinhChung/1511061266')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (2, N'1511061539', N'')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (3, N'1511061000', N'')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (5, N'231', N'')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (6, N'1511061001', N'~/MinhChung/1511061001')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (7, N'1511061538', N'~/MinhChung/1511061538')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (15, N'1511061266', N'~/MinhChung/1511061266')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (16, N'1511061100', N'~/MinhChung/1511061100')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (17, N'1511061500', N'~/MinhChung/1511061500')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (18, N'1511061800', N'~/MinhChung/1511061800')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (19, N'1511061700', N'~/MinhChung/1511061700')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (20, N'1511061600', N'~/MinhChung/1511061600')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (21, N'1511061900', N'~/MinhChung/1511061900')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (23, N'1511062000', N'~/MinhChung/1511062000')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (24, N'1511065150', N'~/MinhChung/1511065150')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (25, N'1511065001', N'~/MinhChung/1511065001')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (26, N'1511065002', N'~/MinhChung/1511065002')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (27, N'1511065005', N'~/MinhChung/1511065005')
INSERT [dbo].[MinhChung] ([MaMinhChung], [TenMInhChung], [URL]) VALUES (28, N'1511065260', N'~/MinhChung/1511065260')
SET IDENTITY_INSERT [dbo].[MinhChung] OFF
SET IDENTITY_INSERT [dbo].[PDK_BaiBao_ThamLuan_NCKH] ON 

INSERT [dbo].[PDK_BaiBao_ThamLuan_NCKH] ([MaPDK_BBao_TL_CKH], [TenBaiBao], [NoiDung], [MSSV]) VALUES (1, N'Báo Thanh niên khỏe', N'S?c kh?e c?a thanh niên', N'1511061266')
INSERT [dbo].[PDK_BaiBao_ThamLuan_NCKH] ([MaPDK_BBao_TL_CKH], [TenBaiBao], [NoiDung], [MSSV]) VALUES (2, N'Báo Thiếu niên khỏe', N'S?c kh?e c?a thi?u niên', N'1511061538')
INSERT [dbo].[PDK_BaiBao_ThamLuan_NCKH] ([MaPDK_BBao_TL_CKH], [TenBaiBao], [NoiDung], [MSSV]) VALUES (3, N'Báo Share', N'Chia s? thông tin', N'1511061000')
SET IDENTITY_INSERT [dbo].[PDK_BaiBao_ThamLuan_NCKH] OFF
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I1B', 1, 0, 0, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I1T', 1, 0, 0, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I2B', 0, 1, 0, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I2T', 0, 1, 0, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I3B', 0, 0, 1, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I3T', 0, 0, 1, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I4B', 0, 0, 0, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'I4T', 0, 0, 0, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II12B', 1, 1, 0, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II12T', 1, 1, 0, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II13B', 1, 0, 1, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II13T', 1, 0, 1, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II14B', 1, 0, 0, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II14T', 1, 0, 0, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II23B', 0, 1, 1, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II23T', 0, 1, 1, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II24B', 0, 1, 0, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II24T', 0, 1, 0, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II34B', 0, 0, 1, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'II34T', 0, 0, 1, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III01B', 0, 1, 1, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III01T', 0, 1, 1, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III02B', 1, 0, 1, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III02T', 1, 0, 1, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III03B', 1, 1, 0, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III03T', 1, 1, 0, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III04B', 1, 1, 1, 0, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'III04T', 1, 1, 1, 0, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'IVB', 1, 1, 1, 1, N'BB')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'IVT', 1, 1, 1, 1, N'BT')
INSERT [dbo].[PhanCap_TieuChi] ([MaPhanCap], [Khoa], [Truong], [Thanh], [TrungUong], [PhanLoai]) VALUES (N'LHTT', 0, 0, 0, 0, N'BT')
INSERT [dbo].[PhanLoai] ([PhanLoai], [BatBuoc]) VALUES (N'BB', 1)
INSERT [dbo].[PhanLoai] ([PhanLoai], [BatBuoc]) VALUES (N'BT', 0)
SET IDENTITY_INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ON 

INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (7, N'CH01', NULL, N'a', CAST(N'2018-12-06T17:15:24.837' AS DateTime), N'1511061266', 0)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (8, N'TG01', NULL, N'a', CAST(N'2018-12-06T17:15:51.507' AS DateTime), N'1511061266', 0)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (11, N'CH01', NULL, N'a', CAST(N'2018-12-24T15:25:09.113' AS DateTime), N'1511065002', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (12, N'CH01', NULL, N'asa', CAST(N'2018-12-24T15:29:41.680' AS DateTime), N'1511065002', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (13, N'CH01', NULL, N'aasda', CAST(N'2018-12-24T15:32:56.470' AS DateTime), N'1511065002', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (14, N'CH01', NULL, N'asd', CAST(N'2018-12-24T15:34:43.073' AS DateTime), N'1511065001', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (15, N'CH01', NULL, N'a', CAST(N'2018-12-24T15:38:10.893' AS DateTime), N'1511065001', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (16, N'CH01', NULL, N'a', CAST(N'2018-12-24T15:40:31.073' AS DateTime), N'1511065001', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (17, N'CH01', NULL, N'a', CAST(N'2018-12-24T15:41:49.647' AS DateTime), N'1511065001', -1)
INSERT [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH], [Ma_GVHD1], [Ma_GVHD2], [TenDeTai], [NgayDK], [ND_DeTai], [TinhTrang]) VALUES (18, N'CH01', NULL, N'Quản lý thông tin sinh viên 5 tốt', CAST(N'2018-12-24T16:02:17.573' AS DateTime), N'1511065005', 1)
SET IDENTITY_INSERT [dbo].[PhieuDangKy_DeTai_NCKH] OFF
SET IDENTITY_INSERT [dbo].[PhieuDangKy_LHTT] ON 

INSERT [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT], [MaMinhChung], [NamHoc], [MSSV]) VALUES (4, 15, 18, N'1511061266')
INSERT [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT], [MaMinhChung], [NamHoc], [MSSV]) VALUES (5, 17, 18, N'1511061500')
INSERT [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT], [MaMinhChung], [NamHoc], [MSSV]) VALUES (7, 19, 2018, N'1511061700')
INSERT [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT], [MaMinhChung], [NamHoc], [MSSV]) VALUES (8, 20, 2018, N'1511061600')
INSERT [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT], [MaMinhChung], [NamHoc], [MSSV]) VALUES (10, 24, 2018, N'1511065150')
INSERT [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT], [MaMinhChung], [NamHoc], [MSSV]) VALUES (11, 28, 2018, N'1511065260')
SET IDENTITY_INSERT [dbo].[PhieuDangKy_LHTT] OFF
SET IDENTITY_INSERT [dbo].[PhieuDangKy_SV5T] ON 

INSERT [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T], [MaMinhChung], [MaCap], [NamHoc], [MSSV]) VALUES (10008, 15, N'TR', 18, N'1511061266')
INSERT [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T], [MaMinhChung], [MaCap], [NamHoc], [MSSV]) VALUES (10009, 3, N'K', 18, N'1511061000')
INSERT [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T], [MaMinhChung], [MaCap], [NamHoc], [MSSV]) VALUES (10014, 23, N'TW', 2018, N'1511062000')
INSERT [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T], [MaMinhChung], [MaCap], [NamHoc], [MSSV]) VALUES (10015, 25, N'K', 2018, N'1511065001')
INSERT [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T], [MaMinhChung], [MaCap], [NamHoc], [MSSV]) VALUES (10016, 26, N'TR', 2018, N'1511065002')
INSERT [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T], [MaMinhChung], [MaCap], [NamHoc], [MSSV]) VALUES (10017, 27, N'K', 2018, N'1511065005')
SET IDENTITY_INSERT [dbo].[PhieuDangKy_SV5T] OFF
INSERT [dbo].[PhongBan] ([MaPhongBan], [TenPhongBan]) VALUES (N'PDT', N'Phòng Đào Tạo')
INSERT [dbo].[PhongBan] ([MaPhongBan], [TenPhongBan]) VALUES (N'CTSV', N'Công Tác Sinh Viên')
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061000', N'15DTH11', N'Nguyễn', N'Đức Phát', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907811997, N'TP.HCM', 1, N'1511061000', N'nguyenphat224@gmail.com', N'TP.HCM', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 0)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061001', N'15DTH12', N'Quoc', N'Cuong', CAST(N'1997-01-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'1511061001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061100', N'15DTH11', N'Cường', N'Cường', CAST(N'1997-01-05T00:00:00.000' AS DateTime), 132456798, N'', 1, N'1511061100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061266', N'15DTH11', N'Dương Thành', N'Phết', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907811997, N'TP.HCM', 1, N'1511061266', N'nguyenphat224@gmail.com', N'TP.HCM', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061500', N'15DTH11', N'A', N'Quốc Cường', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907810062, N'Bến Tre', 1, N'1511061500', N'cuongngao@gmail.com', N'Bến Tre', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061538', N'15DTH11', N'Trường', N'Quốc Cường', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907810062, N'Bến Tre', 1, N'1511061538', N'cuongngao@gmail.com', N'Bến Tre', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 0)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061600', N'15DTH13', N'Nguyễn', N'A', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907810062, N'Bến Tre', 1, N'1511061600', N'cuongngao@gmail.com', N'Bến Tre', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061700', N'15DTH11', N'Nguyễn', N'Minh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907810062, N'Bến Tre', 1, N'1511061700', N'cuongngao@gmail.com', N'Bến Tre', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061800', N'15DTH14', N'Nguyễn', N'Minh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), 907810062, N'Bến Tre', 1, N'1511061800', N'cuongngao@gmail.com', N'Bến Tre', N'120 Nguyễn Trãi, phường Bến Thành, quận 1', N'C2/19RA Phạm Hùng, xã Bình Hưng, huyện Bình Chánh', CAST(N'1900-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511061900', N'15DTH15', N'Trần', N'Vũ', CAST(N'1994-01-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'1511061900', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511062000', N'15DTH10', N'Dương Thành', N'Phết', CAST(N'1997-02-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'1511062000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511063000', N'15DTH20', N'Trần', N'Văn', CAST(N'1996-05-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'1511063000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065001', N'15DTH11', N'Nguyễn', N'1', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'11', 1, N'1511065001', NULL, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065002', N'15DTH11', N'Nguyễn', N'2', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'22', 1, N'1511065002', NULL, N'2', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065003', N'15DTH11', N'Nguyễn', N'3', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'33', 1, N'1511065003', NULL, N'3', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065004', N'15DTH11', N'Nguyễn', N'4', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'44', 1, N'1511065004', NULL, N'4', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065005', N'15DTH11', N'Nguyễn', N'5', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'55', 1, N'1511065005', NULL, N'5', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065006', N'15DTH11', N'Nguyễn', N'6', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'66', 1, N'1511065006', NULL, N'6', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065007', N'15DTH11', N'Nguyễn', N'7', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'77', 1, N'1511065007', NULL, N'7', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065008', N'15DTH11', N'Nguyễn', N'8', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'88', 1, N'1511065008', NULL, N'8', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065009', N'15DTH11', N'Nguyễn', N'9', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'99', 1, N'1511065009', NULL, N'9', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065010', N'15DTH11', N'Nguyễn', N'10', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1010', 1, N'1511065010', NULL, N'10', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065011', N'15DTH11', N'Nguyễn', N'11', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1111', 1, N'1511065011', NULL, N'11', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065012', N'15DTH11', N'Nguyễn', N'12', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1212', 1, N'1511065012', NULL, N'12', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065013', N'15DTH11', N'Nguyễn', N'13', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1313', 1, N'1511065013', NULL, N'13', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065014', N'15DTH11', N'Nguyễn', N'14', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1414', 1, N'1511065014', NULL, N'14', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065015', N'15DTH11', N'Nguyễn', N'15', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1515', 1, N'1511065015', NULL, N'15', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065016', N'15DTH11', N'Nguyễn', N'16', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1616', 1, N'1511065016', NULL, N'16', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065017', N'15DTH11', N'Nguyễn', N'17', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1717', 1, N'1511065017', NULL, N'17', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065018', N'15DTH11', N'Nguyễn', N'18', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1818', 1, N'1511065018', NULL, N'18', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065019', N'15DTH11', N'Nguyễn', N'19', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'1919', 1, N'1511065019', NULL, N'19', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065020', N'15DTH11', N'Nguyễn', N'20', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2020', 1, N'1511065020', NULL, N'20', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065021', N'15DTH11', N'Nguyễn', N'21', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2121', 1, N'1511065021', NULL, N'21', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065022', N'15DTH11', N'Nguyễn', N'22', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2222', 1, N'1511065022', NULL, N'22', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065023', N'15DTH11', N'Nguyễn', N'23', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2323', 1, N'1511065023', NULL, N'23', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065024', N'15DTH11', N'Nguyễn', N'24', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2424', 1, N'1511065024', NULL, N'24', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065025', N'15DTH11', N'Nguyễn', N'25', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2525', 1, N'1511065025', NULL, N'25', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065026', N'15DTH11', N'Nguyễn', N'26', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2626', 1, N'1511065026', NULL, N'26', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065027', N'15DTH11', N'Nguyễn', N'27', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2727', 1, N'1511065027', NULL, N'27', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065028', N'15DTH11', N'Nguyễn', N'28', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2828', 1, N'1511065028', NULL, N'28', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065029', N'15DTH11', N'Nguyễn', N'29', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'2929', 1, N'1511065029', NULL, N'29', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065030', N'15DTH11', N'Nguyễn', N'30', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3030', 1, N'1511065030', NULL, N'30', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065031', N'15DTH11', N'Nguyễn', N'31', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3131', 1, N'1511065031', NULL, N'31', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065032', N'15DTH11', N'Nguyễn', N'32', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3232', 1, N'1511065032', NULL, N'32', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065033', N'15DTH11', N'Nguyễn', N'33', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3333', 1, N'1511065033', NULL, N'33', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065034', N'15DTH11', N'Nguyễn', N'34', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3434', 1, N'1511065034', NULL, N'34', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065035', N'15DTH11', N'Nguyễn', N'35', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3535', 1, N'1511065035', NULL, N'35', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065036', N'15DTH11', N'Nguyễn', N'36', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3636', 1, N'1511065036', NULL, N'36', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065037', N'15DTH11', N'Nguyễn', N'37', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3737', 1, N'1511065037', NULL, N'37', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065038', N'15DTH11', N'Nguyễn', N'38', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3838', 1, N'1511065038', NULL, N'38', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065039', N'15DTH11', N'Nguyễn', N'39', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'3939', 1, N'1511065039', NULL, N'39', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065040', N'15DTH11', N'Nguyễn', N'40', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4040', 1, N'1511065040', NULL, N'40', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065041', N'15DTH11', N'Nguyễn', N'41', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4141', 1, N'1511065041', NULL, N'41', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065042', N'15DTH11', N'Nguyễn', N'42', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4242', 1, N'1511065042', NULL, N'42', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065043', N'15DTH11', N'Nguyễn', N'43', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4343', 1, N'1511065043', NULL, N'43', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065044', N'15DTH11', N'Nguyễn', N'44', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4444', 1, N'1511065044', NULL, N'44', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065045', N'15DTH11', N'Nguyễn', N'45', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4545', 1, N'1511065045', NULL, N'45', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065046', N'15DTH11', N'Nguyễn', N'46', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4646', 1, N'1511065046', NULL, N'46', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065047', N'15DTH11', N'Nguyễn', N'47', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4747', 1, N'1511065047', NULL, N'47', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065048', N'15DTH11', N'Nguyễn', N'48', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4848', 1, N'1511065048', NULL, N'48', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065049', N'15DTH11', N'Nguyễn', N'49', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'4949', 1, N'1511065049', NULL, N'49', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065050', N'15DTH11', N'Nguyễn', N'50', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5050', 1, N'1511065050', NULL, N'50', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065051', N'15DTH11', N'Nguyễn', N'51', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5151', 1, N'1511065051', NULL, N'51', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065052', N'15DTH11', N'Nguyễn', N'52', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5252', 1, N'1511065052', NULL, N'52', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065053', N'15DTH11', N'Nguyễn', N'53', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5353', 1, N'1511065053', NULL, N'53', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065054', N'15DTH11', N'Nguyễn', N'54', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5454', 1, N'1511065054', NULL, N'54', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065055', N'15DTH11', N'Nguyễn', N'55', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5555', 1, N'1511065055', NULL, N'55', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065056', N'15DTH11', N'Nguyễn', N'56', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5656', 1, N'1511065056', NULL, N'56', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065057', N'15DTH11', N'Nguyễn', N'57', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5757', 1, N'1511065057', NULL, N'57', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065058', N'15DTH11', N'Nguyễn', N'58', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5858', 1, N'1511065058', NULL, N'58', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065059', N'15DTH11', N'Nguyễn', N'59', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'5959', 1, N'1511065059', NULL, N'59', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065060', N'15DTH11', N'Nguyễn', N'60', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6060', 1, N'1511065060', NULL, N'60', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065061', N'15DTH11', N'Nguyễn', N'61', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6161', 1, N'1511065061', NULL, N'61', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065062', N'15DTH11', N'Nguyễn', N'62', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6262', 1, N'1511065062', NULL, N'62', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065063', N'15DTH11', N'Nguyễn', N'63', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6363', 1, N'1511065063', NULL, N'63', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065064', N'15DTH11', N'Nguyễn', N'64', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6464', 1, N'1511065064', NULL, N'64', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065065', N'15DTH11', N'Nguyễn', N'65', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6565', 1, N'1511065065', NULL, N'65', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065066', N'15DTH11', N'Nguyễn', N'66', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6666', 1, N'1511065066', NULL, N'66', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065067', N'15DTH11', N'Nguyễn', N'67', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6767', 1, N'1511065067', NULL, N'67', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065068', N'15DTH11', N'Nguyễn', N'68', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6868', 1, N'1511065068', NULL, N'68', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065069', N'15DTH11', N'Nguyễn', N'69', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'6969', 1, N'1511065069', NULL, N'69', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065070', N'15DTH11', N'Nguyễn', N'70', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7070', 1, N'1511065070', NULL, N'70', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065071', N'15DTH11', N'Nguyễn', N'71', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7171', 1, N'1511065071', NULL, N'71', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065072', N'15DTH11', N'Nguyễn', N'72', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7272', 1, N'1511065072', NULL, N'72', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065073', N'15DTH11', N'Nguyễn', N'73', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7373', 1, N'1511065073', NULL, N'73', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065074', N'15DTH11', N'Nguyễn', N'74', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7474', 1, N'1511065074', NULL, N'74', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065075', N'15DTH11', N'Nguyễn', N'75', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7575', 1, N'1511065075', NULL, N'75', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065076', N'15DTH11', N'Nguyễn', N'76', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7676', 1, N'1511065076', NULL, N'76', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065077', N'15DTH11', N'Nguyễn', N'77', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7777', 1, N'1511065077', NULL, N'77', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065078', N'15DTH11', N'Nguyễn', N'78', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7878', 1, N'1511065078', NULL, N'78', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065079', N'15DTH11', N'Nguyễn', N'79', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'7979', 1, N'1511065079', NULL, N'79', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065080', N'15DTH11', N'Nguyễn', N'80', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8080', 1, N'1511065080', NULL, N'80', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065081', N'15DTH11', N'Nguyễn', N'81', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8181', 1, N'1511065081', NULL, N'81', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065082', N'15DTH11', N'Nguyễn', N'82', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8282', 1, N'1511065082', NULL, N'82', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065083', N'15DTH11', N'Nguyễn', N'83', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8383', 1, N'1511065083', NULL, N'83', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065084', N'15DTH11', N'Nguyễn', N'84', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8484', 1, N'1511065084', NULL, N'84', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065085', N'15DTH11', N'Nguyễn', N'85', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8585', 1, N'1511065085', NULL, N'85', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065086', N'15DTH11', N'Nguyễn', N'86', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8686', 1, N'1511065086', NULL, N'86', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065087', N'15DTH11', N'Nguyễn', N'87', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8787', 1, N'1511065087', NULL, N'87', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065088', N'15DTH11', N'Nguyễn', N'88', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8888', 1, N'1511065088', NULL, N'88', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065089', N'15DTH11', N'Nguyễn', N'89', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'8989', 1, N'1511065089', NULL, N'89', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065090', N'15DTH11', N'Nguyễn', N'90', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9090', 1, N'1511065090', NULL, N'90', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065091', N'15DTH11', N'Nguyễn', N'91', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9191', 1, N'1511065091', NULL, N'91', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065092', N'15DTH11', N'Nguyễn', N'92', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9292', 1, N'1511065092', NULL, N'92', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065093', N'15DTH11', N'Nguyễn', N'93', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9393', 1, N'1511065093', NULL, N'93', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065094', N'15DTH11', N'Nguyễn', N'94', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9494', 1, N'1511065094', NULL, N'94', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065095', N'15DTH11', N'Nguyễn', N'95', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9595', 1, N'1511065095', NULL, N'95', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065096', N'15DTH11', N'Nguyễn', N'96', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9696', 1, N'1511065096', NULL, N'96', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065097', N'15DTH11', N'Nguyễn', N'97', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9797', 1, N'1511065097', NULL, N'97', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065098', N'15DTH11', N'Nguyễn', N'98', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9898', 1, N'1511065098', NULL, N'98', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065099', N'15DTH11', N'Nguyễn', N'99', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'9999', 1, N'1511065099', NULL, N'99', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065100', N'15DTH11', N'Nguyễn', N'100', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'100100', 1, N'1511065100', NULL, N'100', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065101', N'15DTH12', N'Nguyễn', N'101', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'101101', 1, N'1511065101', NULL, N'101', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065102', N'15DTH12', N'Nguyễn', N'102', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'102102', 1, N'1511065102', NULL, N'102', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065103', N'15DTH12', N'Nguyễn', N'103', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'103103', 1, N'1511065103', NULL, N'103', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065104', N'15DTH12', N'Nguyễn', N'104', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'104104', 1, N'1511065104', NULL, N'104', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065105', N'15DTH12', N'Nguyễn', N'105', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'105105', 1, N'1511065105', NULL, N'105', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065106', N'15DTH12', N'Nguyễn', N'106', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'106106', 1, N'1511065106', NULL, N'106', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065107', N'15DTH12', N'Nguyễn', N'107', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'107107', 1, N'1511065107', NULL, N'107', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065108', N'15DTH12', N'Nguyễn', N'108', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'108108', 1, N'1511065108', NULL, N'108', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065109', N'15DTH12', N'Nguyễn', N'109', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'109109', 1, N'1511065109', NULL, N'109', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065110', N'15DTH12', N'Nguyễn', N'110', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'110110', 1, N'1511065110', NULL, N'110', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065111', N'15DTH12', N'Nguyễn', N'111', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'111111', 1, N'1511065111', NULL, N'111', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065112', N'15DTH12', N'Nguyễn', N'112', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'112112', 1, N'1511065112', NULL, N'112', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065113', N'15DTH12', N'Nguyễn', N'113', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'113113', 1, N'1511065113', NULL, N'113', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065114', N'15DTH12', N'Nguyễn', N'114', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'114114', 1, N'1511065114', NULL, N'114', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065115', N'15DTH12', N'Nguyễn', N'115', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'115115', 1, N'1511065115', NULL, N'115', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065116', N'15DTH12', N'Nguyễn', N'116', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'116116', 1, N'1511065116', NULL, N'116', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065117', N'15DTH12', N'Nguyễn', N'117', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'117117', 1, N'1511065117', NULL, N'117', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065118', N'15DTH12', N'Nguyễn', N'118', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'118118', 1, N'1511065118', NULL, N'118', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065119', N'15DTH12', N'Nguyễn', N'119', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'119119', 1, N'1511065119', NULL, N'119', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065120', N'15DTH12', N'Nguyễn', N'120', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'120120', 1, N'1511065120', NULL, N'120', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065121', N'15DTH12', N'Nguyễn', N'121', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'121121', 1, N'1511065121', NULL, N'121', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065122', N'15DTH12', N'Nguyễn', N'122', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'122122', 1, N'1511065122', NULL, N'122', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065123', N'15DTH12', N'Nguyễn', N'123', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'123123', 1, N'1511065123', NULL, N'123', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065124', N'15DTH12', N'Nguyễn', N'124', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'124124', 1, N'1511065124', NULL, N'124', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065125', N'15DTH12', N'Nguyễn', N'125', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'125125', 1, N'1511065125', NULL, N'125', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065126', N'15DTH12', N'Nguyễn', N'126', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'126126', 1, N'1511065126', NULL, N'126', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065127', N'15DTH12', N'Nguyễn', N'127', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'127127', 1, N'1511065127', NULL, N'127', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065128', N'15DTH12', N'Nguyễn', N'128', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'128128', 1, N'1511065128', NULL, N'128', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065129', N'15DTH12', N'Nguyễn', N'129', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'129129', 1, N'1511065129', NULL, N'129', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065130', N'15DTH12', N'Nguyễn', N'130', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'130130', 1, N'1511065130', NULL, N'130', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065131', N'15DTH12', N'Nguyễn', N'131', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'131131', 1, N'1511065131', NULL, N'131', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065132', N'15DTH12', N'Nguyễn', N'132', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'132132', 1, N'1511065132', NULL, N'132', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065133', N'15DTH12', N'Nguyễn', N'133', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'133133', 1, N'1511065133', NULL, N'133', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065134', N'15DTH12', N'Nguyễn', N'134', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'134134', 1, N'1511065134', NULL, N'134', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065135', N'15DTH12', N'Nguyễn', N'135', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'135135', 1, N'1511065135', NULL, N'135', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065136', N'15DTH12', N'Nguyễn', N'136', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'136136', 1, N'1511065136', NULL, N'136', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065137', N'15DTH12', N'Nguyễn', N'137', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'137137', 1, N'1511065137', NULL, N'137', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065138', N'15DTH12', N'Nguyễn', N'138', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'138138', 1, N'1511065138', NULL, N'138', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065139', N'15DTH12', N'Nguyễn', N'139', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'139139', 1, N'1511065139', NULL, N'139', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065140', N'15DTH12', N'Nguyễn', N'140', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'140140', 1, N'1511065140', NULL, N'140', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065141', N'15DTH12', N'Nguyễn', N'141', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'141141', 1, N'1511065141', NULL, N'141', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065142', N'15DTH12', N'Nguyễn', N'142', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'142142', 1, N'1511065142', NULL, N'142', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065143', N'15DTH12', N'Nguyễn', N'143', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'143143', 1, N'1511065143', NULL, N'143', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065144', N'15DTH12', N'Nguyễn', N'144', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'144144', 1, N'1511065144', NULL, N'144', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065145', N'15DTH12', N'Nguyễn', N'145', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'145145', 1, N'1511065145', NULL, N'145', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065146', N'15DTH12', N'Nguyễn', N'146', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'146146', 1, N'1511065146', NULL, N'146', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065147', N'15DTH12', N'Nguyễn', N'147', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'147147', 1, N'1511065147', NULL, N'147', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065148', N'15DTH12', N'Nguyễn', N'148', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'148148', 1, N'1511065148', NULL, N'148', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065149', N'15DTH12', N'Nguyễn', N'149', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'149149', 1, N'1511065149', NULL, N'149', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065150', N'15DTH12', N'Nguyễn', N'150', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'150150', 1, N'1511065150', NULL, N'150', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065151', N'15DTH12', N'Nguyễn', N'151', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'151151', 1, N'1511065151', NULL, N'151', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065152', N'15DTH12', N'Nguyễn', N'152', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'152152', 1, N'1511065152', NULL, N'152', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065153', N'15DTH12', N'Nguyễn', N'153', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'153153', 1, N'1511065153', NULL, N'153', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065154', N'15DTH12', N'Nguyễn', N'154', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'154154', 1, N'1511065154', NULL, N'154', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065155', N'15DTH12', N'Nguyễn', N'155', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'155155', 1, N'1511065155', NULL, N'155', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065156', N'15DTH12', N'Nguyễn', N'156', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'156156', 1, N'1511065156', NULL, N'156', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065157', N'15DTH12', N'Nguyễn', N'157', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'157157', 1, N'1511065157', NULL, N'157', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065158', N'15DTH12', N'Nguyễn', N'158', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'158158', 1, N'1511065158', NULL, N'158', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065159', N'15DTH12', N'Nguyễn', N'159', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'159159', 1, N'1511065159', NULL, N'159', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065160', N'15DTH12', N'Nguyễn', N'160', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'160160', 1, N'1511065160', NULL, N'160', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065161', N'15DTH12', N'Nguyễn', N'161', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'161161', 1, N'1511065161', NULL, N'161', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065162', N'15DTH12', N'Nguyễn', N'162', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'162162', 1, N'1511065162', NULL, N'162', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065163', N'15DTH12', N'Nguyễn', N'163', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'163163', 1, N'1511065163', NULL, N'163', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065164', N'15DTH12', N'Nguyễn', N'164', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'164164', 1, N'1511065164', NULL, N'164', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065165', N'15DTH12', N'Nguyễn', N'165', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'165165', 1, N'1511065165', NULL, N'165', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065166', N'15DTH12', N'Nguyễn', N'166', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'166166', 1, N'1511065166', NULL, N'166', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065167', N'15DTH12', N'Nguyễn', N'167', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'167167', 1, N'1511065167', NULL, N'167', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065168', N'15DTH12', N'Nguyễn', N'168', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'168168', 1, N'1511065168', NULL, N'168', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065169', N'15DTH12', N'Nguyễn', N'169', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'169169', 1, N'1511065169', NULL, N'169', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065170', N'15DTH12', N'Nguyễn', N'170', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'170170', 1, N'1511065170', NULL, N'170', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065171', N'15DTH12', N'Nguyễn', N'171', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'171171', 1, N'1511065171', NULL, N'171', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065172', N'15DTH12', N'Nguyễn', N'172', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'172172', 1, N'1511065172', NULL, N'172', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065173', N'15DTH12', N'Nguyễn', N'173', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'173173', 1, N'1511065173', NULL, N'173', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065174', N'15DTH12', N'Nguyễn', N'174', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'174174', 1, N'1511065174', NULL, N'174', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065175', N'15DTH12', N'Nguyễn', N'175', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'175175', 1, N'1511065175', NULL, N'175', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065176', N'15DTH12', N'Nguyễn', N'176', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'176176', 1, N'1511065176', NULL, N'176', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065177', N'15DTH12', N'Nguyễn', N'177', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'177177', 1, N'1511065177', NULL, N'177', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065178', N'15DTH12', N'Nguyễn', N'178', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'178178', 1, N'1511065178', NULL, N'178', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065179', N'15DTH12', N'Nguyễn', N'179', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'179179', 1, N'1511065179', NULL, N'179', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065180', N'15DTH12', N'Nguyễn', N'180', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'180180', 1, N'1511065180', NULL, N'180', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065181', N'15DTH12', N'Nguyễn', N'181', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'181181', 1, N'1511065181', NULL, N'181', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065182', N'15DTH12', N'Nguyễn', N'182', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'182182', 1, N'1511065182', NULL, N'182', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065183', N'15DTH12', N'Nguyễn', N'183', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'183183', 1, N'1511065183', NULL, N'183', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065184', N'15DTH12', N'Nguyễn', N'184', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'184184', 1, N'1511065184', NULL, N'184', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065185', N'15DTH12', N'Nguyễn', N'185', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'185185', 1, N'1511065185', NULL, N'185', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065186', N'15DTH12', N'Nguyễn', N'186', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'186186', 1, N'1511065186', NULL, N'186', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065187', N'15DTH12', N'Nguyễn', N'187', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'187187', 1, N'1511065187', NULL, N'187', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065188', N'15DTH12', N'Nguyễn', N'188', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'188188', 1, N'1511065188', NULL, N'188', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065189', N'15DTH12', N'Nguyễn', N'189', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'189189', 1, N'1511065189', NULL, N'189', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065190', N'15DTH12', N'Nguyễn', N'190', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'190190', 1, N'1511065190', NULL, N'190', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065191', N'15DTH12', N'Nguyễn', N'191', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'191191', 1, N'1511065191', NULL, N'191', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065192', N'15DTH12', N'Nguyễn', N'192', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'192192', 1, N'1511065192', NULL, N'192', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065193', N'15DTH12', N'Nguyễn', N'193', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'193193', 1, N'1511065193', NULL, N'193', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065194', N'15DTH12', N'Nguyễn', N'194', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'194194', 1, N'1511065194', NULL, N'194', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065195', N'15DTH12', N'Nguyễn', N'195', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'195195', 1, N'1511065195', NULL, N'195', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065196', N'15DTH12', N'Nguyễn', N'196', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'196196', 1, N'1511065196', NULL, N'196', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065197', N'15DTH12', N'Nguyễn', N'197', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'197197', 1, N'1511065197', NULL, N'197', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065198', N'15DTH12', N'Nguyễn', N'198', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'198198', 1, N'1511065198', NULL, N'198', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065199', N'15DTH12', N'Nguyễn', N'199', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'199199', 1, N'1511065199', NULL, N'199', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065200', N'15DTH12', N'Nguyễn', N'200', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'200200', 1, N'1511065200', NULL, N'200', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065201', N'16DTHB4', N'Nguyễn', N'201', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'201201', 1, N'1511065201', NULL, N'201', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065202', N'16DTHB4', N'Nguyễn', N'202', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'202202', 1, N'1511065202', NULL, N'202', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065203', N'16DTHB4', N'Nguyễn', N'203', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'203203', 1, N'1511065203', NULL, N'203', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065204', N'16DTHB4', N'Nguyễn', N'204', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'204204', 1, N'1511065204', NULL, N'204', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065205', N'16DTHB4', N'Nguyễn', N'205', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'205205', 1, N'1511065205', NULL, N'205', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065206', N'16DTHB4', N'Nguyễn', N'206', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'206206', 1, N'1511065206', NULL, N'206', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065207', N'16DTHB4', N'Nguyễn', N'207', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'207207', 1, N'1511065207', NULL, N'207', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065208', N'16DTHB4', N'Nguyễn', N'208', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'208208', 1, N'1511065208', NULL, N'208', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065209', N'16DTHB4', N'Nguyễn', N'209', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'209209', 1, N'1511065209', NULL, N'209', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065210', N'16DTHB4', N'Nguyễn', N'210', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'210210', 1, N'1511065210', NULL, N'210', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065211', N'16DTHB4', N'Nguyễn', N'211', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'211211', 1, N'1511065211', NULL, N'211', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065212', N'16DTHB4', N'Nguyễn', N'212', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'212212', 1, N'1511065212', NULL, N'212', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065213', N'16DTHB4', N'Nguyễn', N'213', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'213213', 1, N'1511065213', NULL, N'213', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065214', N'16DTHB4', N'Nguyễn', N'214', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'214214', 1, N'1511065214', NULL, N'214', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065215', N'16DTHB4', N'Nguyễn', N'215', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'215215', 1, N'1511065215', NULL, N'215', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065216', N'16DTHB4', N'Nguyễn', N'216', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'216216', 1, N'1511065216', NULL, N'216', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065217', N'16DTHB4', N'Nguyễn', N'217', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'217217', 1, N'1511065217', NULL, N'217', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065218', N'16DTHB4', N'Nguyễn', N'218', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'218218', 1, N'1511065218', NULL, N'218', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065219', N'16DTHB4', N'Nguyễn', N'219', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'219219', 1, N'1511065219', NULL, N'219', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065220', N'16DTHB4', N'Nguyễn', N'220', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'220220', 1, N'1511065220', NULL, N'220', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065221', N'16DTHB4', N'Nguyễn', N'221', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'221221', 1, N'1511065221', NULL, N'221', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065222', N'16DTHB4', N'Nguyễn', N'222', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'222222', 1, N'1511065222', NULL, N'222', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065223', N'16DTHB4', N'Nguyễn', N'223', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'223223', 1, N'1511065223', NULL, N'223', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065224', N'16DTHB4', N'Nguyễn', N'224', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'224224', 1, N'1511065224', NULL, N'224', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065225', N'16DTHB4', N'Nguyễn', N'225', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'225225', 1, N'1511065225', NULL, N'225', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065226', N'16DTHB4', N'Nguyễn', N'226', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'226226', 1, N'1511065226', NULL, N'226', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065227', N'16DTHB4', N'Nguyễn', N'227', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'227227', 1, N'1511065227', NULL, N'227', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065228', N'16DTHB4', N'Nguyễn', N'228', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'228228', 1, N'1511065228', NULL, N'228', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065229', N'16DTHB4', N'Nguyễn', N'229', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'229229', 1, N'1511065229', NULL, N'229', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065230', N'16DTHB4', N'Nguyễn', N'230', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'230230', 1, N'1511065230', NULL, N'230', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065231', N'16DTHB4', N'Nguyễn', N'231', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'231231', 1, N'1511065231', NULL, N'231', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065232', N'16DTHB4', N'Nguyễn', N'232', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'232232', 1, N'1511065232', NULL, N'232', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065233', N'16DTHB4', N'Nguyễn', N'233', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'233233', 1, N'1511065233', NULL, N'233', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065234', N'16DTHB4', N'Nguyễn', N'234', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'234234', 1, N'1511065234', NULL, N'234', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065235', N'16DTHB4', N'Nguyễn', N'235', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'235235', 1, N'1511065235', NULL, N'235', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065236', N'16DTHB4', N'Nguyễn', N'236', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'236236', 1, N'1511065236', NULL, N'236', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065237', N'16DTHB4', N'Nguyễn', N'237', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'237237', 1, N'1511065237', NULL, N'237', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065238', N'16DTHB4', N'Nguyễn', N'238', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'238238', 1, N'1511065238', NULL, N'238', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065239', N'16DTHB4', N'Nguyễn', N'239', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'239239', 1, N'1511065239', NULL, N'239', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065240', N'16DTHB4', N'Nguyễn', N'240', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'240240', 1, N'1511065240', NULL, N'240', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065241', N'16DTHB4', N'Nguyễn', N'241', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'241241', 1, N'1511065241', NULL, N'241', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065242', N'16DTHB4', N'Nguyễn', N'242', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'242242', 1, N'1511065242', NULL, N'242', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065243', N'16DTHB4', N'Nguyễn', N'243', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'243243', 1, N'1511065243', NULL, N'243', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065244', N'16DTHB4', N'Nguyễn', N'244', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'244244', 1, N'1511065244', NULL, N'244', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065245', N'16DTHB4', N'Nguyễn', N'245', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'245245', 1, N'1511065245', NULL, N'245', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065246', N'16DTHB4', N'Nguyễn', N'246', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'246246', 1, N'1511065246', NULL, N'246', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065247', N'16DTHB4', N'Nguyễn', N'247', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'247247', 1, N'1511065247', NULL, N'247', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065248', N'16DTHB4', N'Nguyễn', N'248', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'248248', 1, N'1511065248', NULL, N'248', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065249', N'16DTHB4', N'Nguyễn', N'249', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'249249', 1, N'1511065249', NULL, N'249', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065250', N'16DTHB4', N'Nguyễn', N'250', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'250250', 1, N'1511065250', NULL, N'250', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065251', N'16DTHB4', N'Nguyễn', N'251', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'251251', 1, N'1511065251', NULL, N'251', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065252', N'16DTHB4', N'Nguyễn', N'252', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'252252', 1, N'1511065252', NULL, N'252', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065253', N'16DTHB4', N'Nguyễn', N'253', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'253253', 1, N'1511065253', NULL, N'253', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065254', N'16DTHB4', N'Nguyễn', N'254', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'254254', 1, N'1511065254', NULL, N'254', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065255', N'16DTHB4', N'Nguyễn', N'255', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'255255', 1, N'1511065255', NULL, N'255', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065256', N'16DTHB4', N'Nguyễn', N'256', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'256256', 1, N'1511065256', NULL, N'256', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065257', N'16DTHB4', N'Nguyễn', N'257', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'257257', 1, N'1511065257', NULL, N'257', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065258', N'16DTHB4', N'Nguyễn', N'258', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'258258', 1, N'1511065258', NULL, N'258', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065259', N'16DTHB4', N'Nguyễn', N'259', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'259259', 1, N'1511065259', NULL, N'259', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065260', N'16DTHB4', N'Nguyễn', N'260', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'260260', 1, N'1511065260', NULL, N'260', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065261', N'16DTHB4', N'Nguyễn', N'261', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'261261', 1, N'1511065261', NULL, N'261', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065262', N'16DTHB4', N'Nguyễn', N'262', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'262262', 1, N'1511065262', NULL, N'262', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065263', N'16DTHB4', N'Nguyễn', N'263', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'263263', 1, N'1511065263', NULL, N'263', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065264', N'16DTHB4', N'Nguyễn', N'264', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'264264', 1, N'1511065264', NULL, N'264', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065265', N'16DTHB4', N'Nguyễn', N'265', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'265265', 1, N'1511065265', NULL, N'265', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065266', N'16DTHB4', N'Nguyễn', N'266', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'266266', 1, N'1511065266', NULL, N'266', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065267', N'16DTHB4', N'Nguyễn', N'267', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'267267', 1, N'1511065267', NULL, N'267', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065268', N'16DTHB4', N'Nguyễn', N'268', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'268268', 1, N'1511065268', NULL, N'268', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065269', N'16DTHB4', N'Nguyễn', N'269', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'269269', 1, N'1511065269', NULL, N'269', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065270', N'16DTHB4', N'Nguyễn', N'270', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'270270', 1, N'1511065270', NULL, N'270', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065271', N'16DTHB4', N'Nguyễn', N'271', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'271271', 1, N'1511065271', NULL, N'271', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065272', N'16DTHB4', N'Nguyễn', N'272', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'272272', 1, N'1511065272', NULL, N'272', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065273', N'16DTHB4', N'Nguyễn', N'273', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'273273', 1, N'1511065273', NULL, N'273', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065274', N'16DTHB4', N'Nguyễn', N'274', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'274274', 1, N'1511065274', NULL, N'274', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065275', N'16DTHB4', N'Nguyễn', N'275', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'275275', 1, N'1511065275', NULL, N'275', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065276', N'16DTHB4', N'Nguyễn', N'276', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'276276', 1, N'1511065276', NULL, N'276', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065277', N'16DTHB4', N'Nguyễn', N'277', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'277277', 1, N'1511065277', NULL, N'277', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065278', N'16DTHB4', N'Nguyễn', N'278', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'278278', 1, N'1511065278', NULL, N'278', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065279', N'16DTHB4', N'Nguyễn', N'279', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'279279', 1, N'1511065279', NULL, N'279', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065280', N'16DTHB4', N'Nguyễn', N'280', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'280280', 1, N'1511065280', NULL, N'280', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065281', N'16DTHB4', N'Nguyễn', N'281', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'281281', 1, N'1511065281', NULL, N'281', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065282', N'16DTHB4', N'Nguyễn', N'282', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'282282', 1, N'1511065282', NULL, N'282', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065283', N'16DTHB4', N'Nguyễn', N'283', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'283283', 1, N'1511065283', NULL, N'283', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065284', N'16DTHB4', N'Nguyễn', N'284', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'284284', 1, N'1511065284', NULL, N'284', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065285', N'16DTHB4', N'Nguyễn', N'285', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'285285', 1, N'1511065285', NULL, N'285', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065286', N'16DTHB4', N'Nguyễn', N'286', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'286286', 1, N'1511065286', NULL, N'286', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065287', N'16DTHB4', N'Nguyễn', N'287', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'287287', 1, N'1511065287', NULL, N'287', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065288', N'16DTHB4', N'Nguyễn', N'288', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'288288', 1, N'1511065288', NULL, N'288', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065289', N'16DTHB4', N'Nguyễn', N'289', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'289289', 1, N'1511065289', NULL, N'289', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065290', N'16DTHB4', N'Nguyễn', N'290', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'290290', 1, N'1511065290', NULL, N'290', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065291', N'16DTHB4', N'Nguyễn', N'291', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'291291', 1, N'1511065291', NULL, N'291', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065292', N'16DTHB4', N'Nguyễn', N'292', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'292292', 1, N'1511065292', NULL, N'292', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065293', N'16DTHB4', N'Nguyễn', N'293', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'293293', 1, N'1511065293', NULL, N'293', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065294', N'16DTHB4', N'Nguyễn', N'294', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'294294', 1, N'1511065294', NULL, N'294', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065295', N'16DTHB4', N'Nguyễn', N'295', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'295295', 1, N'1511065295', NULL, N'295', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065296', N'16DTHB4', N'Nguyễn', N'296', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'296296', 1, N'1511065296', NULL, N'296', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065297', N'16DTHB4', N'Nguyễn', N'297', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'297297', 1, N'1511065297', NULL, N'297', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065298', N'16DTHB4', N'Nguyễn', N'298', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'298298', 1, N'1511065298', NULL, N'298', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065299', N'16DTHB4', N'Nguyễn', N'299', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'299299', 1, N'1511065299', NULL, N'299', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065300', N'16DTHB4', N'Nguyễn', N'300', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'300300', 1, N'1511065300', NULL, N'300', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065301', N'17DTHC2', N'Nguyễn', N'301', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'301301', 1, N'1511065301', NULL, N'301', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065302', N'17DTHC2', N'Nguyễn', N'302', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'302302', 1, N'1511065302', NULL, N'302', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065303', N'17DTHC2', N'Nguyễn', N'303', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'303303', 1, N'1511065303', NULL, N'303', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065304', N'17DTHC2', N'Nguyễn', N'304', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'304304', 1, N'1511065304', NULL, N'304', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065305', N'17DTHC2', N'Nguyễn', N'305', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'305305', 1, N'1511065305', NULL, N'305', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065306', N'17DTHC2', N'Nguyễn', N'306', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'306306', 1, N'1511065306', NULL, N'306', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065307', N'17DTHC2', N'Nguyễn', N'307', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'307307', 1, N'1511065307', NULL, N'307', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065308', N'17DTHC2', N'Nguyễn', N'308', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'308308', 1, N'1511065308', NULL, N'308', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065309', N'17DTHC2', N'Nguyễn', N'309', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'309309', 1, N'1511065309', NULL, N'309', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065310', N'17DTHC2', N'Nguyễn', N'310', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'310310', 1, N'1511065310', NULL, N'310', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065311', N'17DTHC2', N'Nguyễn', N'311', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'311311', 1, N'1511065311', NULL, N'311', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065312', N'17DTHC2', N'Nguyễn', N'312', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'312312', 1, N'1511065312', NULL, N'312', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065313', N'17DTHC2', N'Nguyễn', N'313', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'313313', 1, N'1511065313', NULL, N'313', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065314', N'17DTHC2', N'Nguyễn', N'314', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'314314', 1, N'1511065314', NULL, N'314', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065315', N'17DTHC2', N'Nguyễn', N'315', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'315315', 1, N'1511065315', NULL, N'315', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065316', N'17DTHC2', N'Nguyễn', N'316', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'316316', 1, N'1511065316', NULL, N'316', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065317', N'17DTHC2', N'Nguyễn', N'317', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'317317', 1, N'1511065317', NULL, N'317', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065318', N'17DTHC2', N'Nguyễn', N'318', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'318318', 1, N'1511065318', NULL, N'318', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065319', N'17DTHC2', N'Nguyễn', N'319', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'319319', 1, N'1511065319', NULL, N'319', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065320', N'17DTHC2', N'Nguyễn', N'320', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'320320', 1, N'1511065320', NULL, N'320', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065321', N'17DTHC2', N'Nguyễn', N'321', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'321321', 1, N'1511065321', NULL, N'321', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065322', N'17DTHC2', N'Nguyễn', N'322', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'322322', 1, N'1511065322', NULL, N'322', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065323', N'17DTHC2', N'Nguyễn', N'323', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'323323', 1, N'1511065323', NULL, N'323', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065324', N'17DTHC2', N'Nguyễn', N'324', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'324324', 1, N'1511065324', NULL, N'324', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065325', N'17DTHC2', N'Nguyễn', N'325', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'325325', 1, N'1511065325', NULL, N'325', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065326', N'17DTHC2', N'Nguyễn', N'326', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'326326', 1, N'1511065326', NULL, N'326', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065327', N'17DTHC2', N'Nguyễn', N'327', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'327327', 1, N'1511065327', NULL, N'327', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065328', N'17DTHC2', N'Nguyễn', N'328', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'328328', 1, N'1511065328', NULL, N'328', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065329', N'17DTHC2', N'Nguyễn', N'329', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'329329', 1, N'1511065329', NULL, N'329', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065330', N'17DTHC2', N'Nguyễn', N'330', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'330330', 1, N'1511065330', NULL, N'330', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065331', N'17DTHC2', N'Nguyễn', N'331', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'331331', 1, N'1511065331', NULL, N'331', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065332', N'17DTHC2', N'Nguyễn', N'332', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'332332', 1, N'1511065332', NULL, N'332', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065333', N'17DTHC2', N'Nguyễn', N'333', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'333333', 1, N'1511065333', NULL, N'333', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065334', N'17DTHC2', N'Nguyễn', N'334', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'334334', 1, N'1511065334', NULL, N'334', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065335', N'17DTHC2', N'Nguyễn', N'335', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'335335', 1, N'1511065335', NULL, N'335', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065336', N'17DTHC2', N'Nguyễn', N'336', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'336336', 1, N'1511065336', NULL, N'336', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065337', N'17DTHC2', N'Nguyễn', N'337', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'337337', 1, N'1511065337', NULL, N'337', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065338', N'17DTHC2', N'Nguyễn', N'338', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'338338', 1, N'1511065338', NULL, N'338', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065339', N'17DTHC2', N'Nguyễn', N'339', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'339339', 1, N'1511065339', NULL, N'339', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065340', N'17DTHC2', N'Nguyễn', N'340', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'340340', 1, N'1511065340', NULL, N'340', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065341', N'17DTHC2', N'Nguyễn', N'341', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'341341', 1, N'1511065341', NULL, N'341', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065342', N'17DTHC2', N'Nguyễn', N'342', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'342342', 1, N'1511065342', NULL, N'342', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065343', N'17DTHC2', N'Nguyễn', N'343', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'343343', 1, N'1511065343', NULL, N'343', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065344', N'17DTHC2', N'Nguyễn', N'344', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'344344', 1, N'1511065344', NULL, N'344', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065345', N'17DTHC2', N'Nguyễn', N'345', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'345345', 1, N'1511065345', NULL, N'345', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065346', N'17DTHC2', N'Nguyễn', N'346', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'346346', 1, N'1511065346', NULL, N'346', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065347', N'17DTHC2', N'Nguyễn', N'347', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'347347', 1, N'1511065347', NULL, N'347', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065348', N'17DTHC2', N'Nguyễn', N'348', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'348348', 1, N'1511065348', NULL, N'348', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065349', N'17DTHC2', N'Nguyễn', N'349', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'349349', 1, N'1511065349', NULL, N'349', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065350', N'17DTHC2', N'Nguyễn', N'350', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'350350', 1, N'1511065350', NULL, N'350', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065351', N'17DTHC2', N'Nguyễn', N'351', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'351351', 1, N'1511065351', NULL, N'351', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065352', N'17DTHC2', N'Nguyễn', N'352', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'352352', 1, N'1511065352', NULL, N'352', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065353', N'17DTHC2', N'Nguyễn', N'353', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'353353', 1, N'1511065353', NULL, N'353', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065354', N'17DTHC2', N'Nguyễn', N'354', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'354354', 1, N'1511065354', NULL, N'354', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065355', N'17DTHC2', N'Nguyễn', N'355', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'355355', 1, N'1511065355', NULL, N'355', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065356', N'17DTHC2', N'Nguyễn', N'356', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'356356', 1, N'1511065356', NULL, N'356', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065357', N'17DTHC2', N'Nguyễn', N'357', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'357357', 1, N'1511065357', NULL, N'357', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065358', N'17DTHC2', N'Nguyễn', N'358', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'358358', 1, N'1511065358', NULL, N'358', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065359', N'17DTHC2', N'Nguyễn', N'359', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'359359', 1, N'1511065359', NULL, N'359', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065360', N'17DTHC2', N'Nguyễn', N'360', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'360360', 1, N'1511065360', NULL, N'360', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065361', N'17DTHC2', N'Nguyễn', N'361', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'361361', 1, N'1511065361', NULL, N'361', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065362', N'17DTHC2', N'Nguyễn', N'362', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'362362', 1, N'1511065362', NULL, N'362', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065363', N'17DTHC2', N'Nguyễn', N'363', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'363363', 1, N'1511065363', NULL, N'363', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065364', N'17DTHC2', N'Nguyễn', N'364', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'364364', 1, N'1511065364', NULL, N'364', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065365', N'17DTHC2', N'Nguyễn', N'365', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'365365', 1, N'1511065365', NULL, N'365', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065366', N'17DTHC2', N'Nguyễn', N'366', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'366366', 1, N'1511065366', NULL, N'366', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065367', N'17DTHC2', N'Nguyễn', N'367', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'367367', 1, N'1511065367', NULL, N'367', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065368', N'17DTHC2', N'Nguyễn', N'368', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'368368', 1, N'1511065368', NULL, N'368', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065369', N'17DTHC2', N'Nguyễn', N'369', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'369369', 1, N'1511065369', NULL, N'369', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065370', N'17DTHC2', N'Nguyễn', N'370', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'370370', 1, N'1511065370', NULL, N'370', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065371', N'17DTHC2', N'Nguyễn', N'371', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'371371', 1, N'1511065371', NULL, N'371', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065372', N'17DTHC2', N'Nguyễn', N'372', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'372372', 1, N'1511065372', NULL, N'372', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065373', N'17DTHC2', N'Nguyễn', N'373', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'373373', 1, N'1511065373', NULL, N'373', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065374', N'17DTHC2', N'Nguyễn', N'374', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'374374', 1, N'1511065374', NULL, N'374', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065375', N'17DTHC2', N'Nguyễn', N'375', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'375375', 1, N'1511065375', NULL, N'375', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065376', N'17DTHC2', N'Nguyễn', N'376', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'376376', 1, N'1511065376', NULL, N'376', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065377', N'17DTHC2', N'Nguyễn', N'377', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'377377', 1, N'1511065377', NULL, N'377', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065378', N'17DTHC2', N'Nguyễn', N'378', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'378378', 1, N'1511065378', NULL, N'378', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065379', N'17DTHC2', N'Nguyễn', N'379', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'379379', 1, N'1511065379', NULL, N'379', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065380', N'17DTHC2', N'Nguyễn', N'380', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'380380', 1, N'1511065380', NULL, N'380', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065381', N'17DTHC2', N'Nguyễn', N'381', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'381381', 1, N'1511065381', NULL, N'381', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065382', N'17DTHC2', N'Nguyễn', N'382', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'382382', 1, N'1511065382', NULL, N'382', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065383', N'17DTHC2', N'Nguyễn', N'383', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'383383', 1, N'1511065383', NULL, N'383', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065384', N'17DTHC2', N'Nguyễn', N'384', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'384384', 1, N'1511065384', NULL, N'384', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065385', N'17DTHC2', N'Nguyễn', N'385', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'385385', 1, N'1511065385', NULL, N'385', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065386', N'17DTHC2', N'Nguyễn', N'386', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'386386', 1, N'1511065386', NULL, N'386', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065387', N'17DTHC2', N'Nguyễn', N'387', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'387387', 1, N'1511065387', NULL, N'387', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065388', N'17DTHC2', N'Nguyễn', N'388', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'388388', 1, N'1511065388', NULL, N'388', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065389', N'17DTHC2', N'Nguyễn', N'389', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'389389', 1, N'1511065389', NULL, N'389', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065390', N'17DTHC2', N'Nguyễn', N'390', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'390390', 1, N'1511065390', NULL, N'390', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065391', N'17DTHC2', N'Nguyễn', N'391', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'391391', 1, N'1511065391', NULL, N'391', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065392', N'17DTHC2', N'Nguyễn', N'392', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'392392', 1, N'1511065392', NULL, N'392', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065393', N'17DTHC2', N'Nguyễn', N'393', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'393393', 1, N'1511065393', NULL, N'393', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065394', N'17DTHC2', N'Nguyễn', N'394', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'394394', 1, N'1511065394', NULL, N'394', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065395', N'17DTHC2', N'Nguyễn', N'395', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'395395', 1, N'1511065395', NULL, N'395', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065396', N'17DTHC2', N'Nguyễn', N'396', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'396396', 1, N'1511065396', NULL, N'396', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065397', N'17DTHC2', N'Nguyễn', N'397', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'397397', 1, N'1511065397', NULL, N'397', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065398', N'17DTHC2', N'Nguyễn', N'398', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'398398', 1, N'1511065398', NULL, N'398', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065399', N'17DTHC2', N'Nguyễn', N'399', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'399399', 1, N'1511065399', NULL, N'399', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065400', N'17DTHC2', N'Nguyễn', N'400', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'400400', 1, N'1511065400', NULL, N'400', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065401', N'18DTH02', N'Nguyễn', N'401', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'401401', 1, N'1511065401', NULL, N'401', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065402', N'18DTH02', N'Nguyễn', N'402', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'402402', 1, N'1511065402', NULL, N'402', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065403', N'18DTH02', N'Nguyễn', N'403', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'403403', 1, N'1511065403', NULL, N'403', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065404', N'18DTH02', N'Nguyễn', N'404', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'404404', 1, N'1511065404', NULL, N'404', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065405', N'18DTH02', N'Nguyễn', N'405', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'405405', 1, N'1511065405', NULL, N'405', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065406', N'18DTH02', N'Nguyễn', N'406', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'406406', 1, N'1511065406', NULL, N'406', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065407', N'18DTH02', N'Nguyễn', N'407', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'407407', 1, N'1511065407', NULL, N'407', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065408', N'18DTH02', N'Nguyễn', N'408', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'408408', 1, N'1511065408', NULL, N'408', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065409', N'18DTH02', N'Nguyễn', N'409', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'409409', 1, N'1511065409', NULL, N'409', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065410', N'18DTH02', N'Nguyễn', N'410', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'410410', 1, N'1511065410', NULL, N'410', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065411', N'18DTH02', N'Nguyễn', N'411', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'411411', 1, N'1511065411', NULL, N'411', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065412', N'18DTH02', N'Nguyễn', N'412', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'412412', 1, N'1511065412', NULL, N'412', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065413', N'18DTH02', N'Nguyễn', N'413', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'413413', 1, N'1511065413', NULL, N'413', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065414', N'18DTH02', N'Nguyễn', N'414', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'414414', 1, N'1511065414', NULL, N'414', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065415', N'18DTH02', N'Nguyễn', N'415', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'415415', 1, N'1511065415', NULL, N'415', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065416', N'18DTH02', N'Nguyễn', N'416', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'416416', 1, N'1511065416', NULL, N'416', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065417', N'18DTH02', N'Nguyễn', N'417', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'417417', 1, N'1511065417', NULL, N'417', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065418', N'18DTH02', N'Nguyễn', N'418', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'418418', 1, N'1511065418', NULL, N'418', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065419', N'18DTH02', N'Nguyễn', N'419', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'419419', 1, N'1511065419', NULL, N'419', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065420', N'18DTH02', N'Nguyễn', N'420', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'420420', 1, N'1511065420', NULL, N'420', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065421', N'18DTH02', N'Nguyễn', N'421', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'421421', 1, N'1511065421', NULL, N'421', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065422', N'18DTH02', N'Nguyễn', N'422', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'422422', 1, N'1511065422', NULL, N'422', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065423', N'18DTH02', N'Nguyễn', N'423', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'423423', 1, N'1511065423', NULL, N'423', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065424', N'18DTH02', N'Nguyễn', N'424', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'424424', 1, N'1511065424', NULL, N'424', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065425', N'18DTH02', N'Nguyễn', N'425', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'425425', 1, N'1511065425', NULL, N'425', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065426', N'18DTH02', N'Nguyễn', N'426', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'426426', 1, N'1511065426', NULL, N'426', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065427', N'18DTH02', N'Nguyễn', N'427', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'427427', 1, N'1511065427', NULL, N'427', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065428', N'18DTH02', N'Nguyễn', N'428', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'428428', 1, N'1511065428', NULL, N'428', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065429', N'18DTH02', N'Nguyễn', N'429', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'429429', 1, N'1511065429', NULL, N'429', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065430', N'18DTH02', N'Nguyễn', N'430', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'430430', 1, N'1511065430', NULL, N'430', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065431', N'18DTH02', N'Nguyễn', N'431', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'431431', 1, N'1511065431', NULL, N'431', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065432', N'18DTH02', N'Nguyễn', N'432', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'432432', 1, N'1511065432', NULL, N'432', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065433', N'18DTH02', N'Nguyễn', N'433', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'433433', 1, N'1511065433', NULL, N'433', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065434', N'18DTH02', N'Nguyễn', N'434', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'434434', 1, N'1511065434', NULL, N'434', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065435', N'18DTH02', N'Nguyễn', N'435', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'435435', 1, N'1511065435', NULL, N'435', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065436', N'18DTH02', N'Nguyễn', N'436', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'436436', 1, N'1511065436', NULL, N'436', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065437', N'18DTH02', N'Nguyễn', N'437', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'437437', 1, N'1511065437', NULL, N'437', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065438', N'18DTH02', N'Nguyễn', N'438', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'438438', 1, N'1511065438', NULL, N'438', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065439', N'18DTH02', N'Nguyễn', N'439', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'439439', 1, N'1511065439', NULL, N'439', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065440', N'18DTH02', N'Nguyễn', N'440', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'440440', 1, N'1511065440', NULL, N'440', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065441', N'18DTH02', N'Nguyễn', N'441', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'441441', 1, N'1511065441', NULL, N'441', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065442', N'18DTH02', N'Nguyễn', N'442', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'442442', 1, N'1511065442', NULL, N'442', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065443', N'18DTH02', N'Nguyễn', N'443', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'443443', 1, N'1511065443', NULL, N'443', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065444', N'18DTH02', N'Nguyễn', N'444', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'444444', 1, N'1511065444', NULL, N'444', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065445', N'18DTH02', N'Nguyễn', N'445', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'445445', 1, N'1511065445', NULL, N'445', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065446', N'18DTH02', N'Nguyễn', N'446', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'446446', 1, N'1511065446', NULL, N'446', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065447', N'18DTH02', N'Nguyễn', N'447', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'447447', 1, N'1511065447', NULL, N'447', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065448', N'18DTH02', N'Nguyễn', N'448', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'448448', 1, N'1511065448', NULL, N'448', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065449', N'18DTH02', N'Nguyễn', N'449', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'449449', 1, N'1511065449', NULL, N'449', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065450', N'18DTH02', N'Nguyễn', N'450', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'450450', 1, N'1511065450', NULL, N'450', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065451', N'18DTH02', N'Nguyễn', N'451', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'451451', 1, N'1511065451', NULL, N'451', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065452', N'18DTH02', N'Nguyễn', N'452', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'452452', 1, N'1511065452', NULL, N'452', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065453', N'18DTH02', N'Nguyễn', N'453', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'453453', 1, N'1511065453', NULL, N'453', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065454', N'18DTH02', N'Nguyễn', N'454', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'454454', 1, N'1511065454', NULL, N'454', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065455', N'18DTH02', N'Nguyễn', N'455', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'455455', 1, N'1511065455', NULL, N'455', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065456', N'18DTH02', N'Nguyễn', N'456', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'456456', 1, N'1511065456', NULL, N'456', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065457', N'18DTH02', N'Nguyễn', N'457', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'457457', 1, N'1511065457', NULL, N'457', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065458', N'18DTH02', N'Nguyễn', N'458', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'458458', 1, N'1511065458', NULL, N'458', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065459', N'18DTH02', N'Nguyễn', N'459', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'459459', 1, N'1511065459', NULL, N'459', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065460', N'18DTH02', N'Nguyễn', N'460', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'460460', 1, N'1511065460', NULL, N'460', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065461', N'18DTH02', N'Nguyễn', N'461', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'461461', 1, N'1511065461', NULL, N'461', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065462', N'18DTH02', N'Nguyễn', N'462', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'462462', 1, N'1511065462', NULL, N'462', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065463', N'18DTH02', N'Nguyễn', N'463', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'463463', 1, N'1511065463', NULL, N'463', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065464', N'18DTH02', N'Nguyễn', N'464', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'464464', 1, N'1511065464', NULL, N'464', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065465', N'18DTH02', N'Nguyễn', N'465', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'465465', 1, N'1511065465', NULL, N'465', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065466', N'18DTH02', N'Nguyễn', N'466', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'466466', 1, N'1511065466', NULL, N'466', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065467', N'18DTH02', N'Nguyễn', N'467', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'467467', 1, N'1511065467', NULL, N'467', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065468', N'18DTH02', N'Nguyễn', N'468', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'468468', 1, N'1511065468', NULL, N'468', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065469', N'18DTH02', N'Nguyễn', N'469', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'469469', 1, N'1511065469', NULL, N'469', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065470', N'18DTH02', N'Nguyễn', N'470', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'470470', 1, N'1511065470', NULL, N'470', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065471', N'18DTH02', N'Nguyễn', N'471', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'471471', 1, N'1511065471', NULL, N'471', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065472', N'18DTH02', N'Nguyễn', N'472', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'472472', 1, N'1511065472', NULL, N'472', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065473', N'18DTH02', N'Nguyễn', N'473', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'473473', 1, N'1511065473', NULL, N'473', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065474', N'18DTH02', N'Nguyễn', N'474', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'474474', 1, N'1511065474', NULL, N'474', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065475', N'18DTH02', N'Nguyễn', N'475', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'475475', 1, N'1511065475', NULL, N'475', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065476', N'18DTH02', N'Nguyễn', N'476', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'476476', 1, N'1511065476', NULL, N'476', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065477', N'18DTH02', N'Nguyễn', N'477', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'477477', 1, N'1511065477', NULL, N'477', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065478', N'18DTH02', N'Nguyễn', N'478', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'478478', 1, N'1511065478', NULL, N'478', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065479', N'18DTH02', N'Nguyễn', N'479', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'479479', 1, N'1511065479', NULL, N'479', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065480', N'18DTH02', N'Nguyễn', N'480', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'480480', 1, N'1511065480', NULL, N'480', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065481', N'18DTH02', N'Nguyễn', N'481', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'481481', 1, N'1511065481', NULL, N'481', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065482', N'18DTH02', N'Nguyễn', N'482', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'482482', 1, N'1511065482', NULL, N'482', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065483', N'18DTH02', N'Nguyễn', N'483', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'483483', 1, N'1511065483', NULL, N'483', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065484', N'18DTH02', N'Nguyễn', N'484', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'484484', 1, N'1511065484', NULL, N'484', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065485', N'18DTH02', N'Nguyễn', N'485', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'485485', 1, N'1511065485', NULL, N'485', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065486', N'18DTH02', N'Nguyễn', N'486', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'486486', 1, N'1511065486', NULL, N'486', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065487', N'18DTH02', N'Nguyễn', N'487', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'487487', 1, N'1511065487', NULL, N'487', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065488', N'18DTH02', N'Nguyễn', N'488', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'488488', 1, N'1511065488', NULL, N'488', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065489', N'18DTH02', N'Nguyễn', N'489', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'489489', 1, N'1511065489', NULL, N'489', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065490', N'18DTH02', N'Nguyễn', N'490', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'490490', 1, N'1511065490', NULL, N'490', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065491', N'18DTH02', N'Nguyễn', N'491', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'491491', 1, N'1511065491', NULL, N'491', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065492', N'18DTH02', N'Nguyễn', N'492', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'492492', 1, N'1511065492', NULL, N'492', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065493', N'18DTH02', N'Nguyễn', N'493', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'493493', 1, N'1511065493', NULL, N'493', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065494', N'18DTH02', N'Nguyễn', N'494', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'494494', 1, N'1511065494', NULL, N'494', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065495', N'18DTH02', N'Nguyễn', N'495', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'495495', 1, N'1511065495', NULL, N'495', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065496', N'18DTH02', N'Nguyễn', N'496', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'496496', 1, N'1511065496', NULL, N'496', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065497', N'18DTH02', N'Nguyễn', N'497', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'497497', 1, N'1511065497', NULL, N'497', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065498', N'18DTH02', N'Nguyễn', N'498', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'498498', 1, N'1511065498', NULL, N'498', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065499', N'18DTH02', N'Nguyễn', N'499', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'499499', 1, N'1511065499', NULL, N'499', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SinhVien] ([MSSV], [MaLop], [Ho], [Ten], [NgaySinh], [SDT], [QueQuan], [GioiTinh], [password], [Email], [NoiSinh], [DiaChiCuTru], [DiaChiLienLac], [NgayKetNapDoan], [NgayVaoDang_dubi_neuco], [NgayVaoDang_chinhthuc_neuco], [ChucVuDoan_neuco], [loptruong]) VALUES (N'1511065500', N'18DTH02', N'Nguyễn', N'500', CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, N'500500', 1, N'1511065500', NULL, N'500', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'Admin', N'Admin', 1)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'CH01', N'CH01', 4)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'CNTP01', N'CNTP01', 3)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'CNTT01', N'CNTT01', 3)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'CTSV01', N'CTSV01', 2)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'KT01', N'KT01', 3)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'MT01', N'MT01', 3)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'OTO01', N'OTO01', 3)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'PDT01', N'PDT01', 2)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'QTKD01', N'QTKD01', 3)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'TG01', N'TG01', 4)
INSERT [dbo].[TaiKhoan_ADMIN] ([TaiKhoan], [MatKhau], [PhanCap]) VALUES (N'TG02', N'TG02', 4)
INSERT [dbo].[ThoiGianHD] ([TenHD], [TG_BatDau], [TG_KetThuc], [GhiChu]) VALUES (N'LHTT', CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'3000-01-01T00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[ThoiGianHD] ([TenHD], [TG_BatDau], [TG_KetThuc], [GhiChu]) VALUES (N'NCKH', CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'3000-01-01T00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[ThoiGianHD] ([TenHD], [TG_BatDau], [TG_KetThuc], [GhiChu]) VALUES (N'SV5T', CAST(N'1900-01-01T00:00:00.000' AS DateTime), CAST(N'3000-01-01T00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'B1', N'HOINHAP', N'Đạt Chứng Chỉ B1', 0, N'III02T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'BDKT', N'UUTIEN', N'Thanh Niên Tiêu Biểu Được Biểu DƯơng Khen Thưởng', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'BViet01', N'HOCTAP', N'Có Bài Viết trên tạp chí từ cấp Khoa', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'BViet02', N'HOCTAP', N'Có Bài Viết trên tạp chí từ cấp trường', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DANG', N'UUTIEN', N'Là Đoàn Viên Được Kết Nạp Đảng', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DGHT02', N'THELUC', N'Đạt Giải Hội Thao từ cấp Khoa', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DGHT03', N'THELUC', N'Đạt Giải Hội Thao từ cấp Thành Phố', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DGHT04', N'HOINHAP', N'Đạt Giải Ba Trở Lên Cuộc Thi Kiến Thức Ngoại Ngữ từ cấp trường', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DGHT05', N'HOINHAP', N'Đạt Giải Ba Trở Lên Cuộc Thi Kiến Thức Ngoại Ngữ từ cấp Khoa', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DHT01', N'HOCTAP', N'Điểm Năm Học', 3, N'I2B')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DHT02', N'HOCTAP', N'Điểm Năm Học', 3.4, N'II34B')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DRL', N'DAODUC', N'Điểm Rèn Luyện', 80, N'III02B')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'DVXS', N'DAODUC', N'Là Đoàn Viên Xuất Sắc', 0, N'III02T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'GTHT01', N'HOCTAP', N'Đạt Giải Cuộc Thi Học Thuật', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'GTST', N'HOCTAP', N'Đạt Giải Cuộc Ý Tưởng Sáng Tạo Cấp Thành', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'GTTV', N'UUTIEN', N'Giới Thiệu 1 Thành Viên Tham Gia Đoàn - Hội', 0, N'III02T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDDC', N'DAODUC', N'Có Hành Động Dũng Cảm', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDGLQT', N'HOINHAP', N'Tham Gia 1 Hoạt Động Giao Lưu Quốc Tế', 0, N'III01T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDHN01', N'HOINHAP', N'Tham Gia 1 Hoạt Động Hội Nhập từ cấp Khoa tổ chức', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDHN02', N'HOINHAP', N'Tham Gia 1 Hoạt Động Hội Nhập từ cấp trường tổ chức', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDTN01', N'TINHNGUYEN', N'Tham Gia 1 Chiến Dịch Do Đoàn - Hội tổ chức', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDTN02', N'TINHNGUYEN', N'Tham Gia Tối Thiểu 5 HĐ/năm', 0, N'III02T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDTN03', N'TINHNGUYEN', N'Được Khen Thưởng Từ Cấp Trường về Hoạt Động Tình Nguyện', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HDTN04', N'TINHNGUYEN', N'Giấy Chứng Nhận: Mùa Hè Xanh, Xuân Tình Nguyện, Tiếp Sức Mùa Thi', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HMTN01', N'UUTIEN', N'Tham Gia Hiến Máu Tình Nguyện', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HMTN02', N'UUTIEN', N'Tham Gia Hiến Máu Tình Nguyện Ít Nhất 2 Lần', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HT1', N'HOCTAP', N'Có 80% sinh viên đi học, đọc tài liệu ở nhà, có nghe giảng và tích cực tham gia thảo luận ở lớp', 12, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HT2', N'HOCTAP', N'Có > 2 sinh viên đạt "Sinh viên 5 tốt" các cấp', 5, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HT3', N'HOCTAP', N'Không có sinh viên vi phạm qui chế thi', 5, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HT4', N'HOCTAP', N'Có > 30% sinh viên đạt điểm trung bình năm học từ loại khá trở lên', 15, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HT5', N'HOCTAP', N'Lớp có tổ chức 1 buổi sinh hoạt chuyên đề hoặc có sinh viên tham gia cuộc thi học thuật, nghiên cứu khoa học từ cấp Trường trở lên', 8, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'HTMac', N'DAODUC', N'Thành Viên Chính Thức Đội Thi Tìm Hiểu Chủ Nghĩa Mác - Lênin', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'KN01', N'HOINHAP', N'Hoàn Thành 1 Kháo Kỹ Năng Hoặc 3 Hội Thảo Kỹ Năng', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'KN02', N'HOINHAP', N'Hoàn Thành 01 khóa Kỹ Năng Thực Hành Xã Hội', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'KT01', N'HOINHAP', N'Khen Thưởng Xuất Sắc CÔng Tác Đoàn - Hội', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'KYNANG1', N'RENLUYEN', N'Lớp tổ chức ít nhất 2 buổi sinh hoạt kỹ năng ngoại khóa', 10, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'KYNANG2', N'RENLUYEN', N'Có > 70% sinh viên lớp tham gia ít nhất 1 buổi hội thảo hoặc tập huấn kỹ năng do Trường/Khoa tổ chức (có giấy xác nhận của Trường/Khoa)', 10, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'NCKH01', N'HOCTAP', N'Đề Tài Nghiên Cứu Khoa Hoc từ cấp khoa', 8, N'I2B')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'NCKH02', N'HOCTAP', N'Đề Tài NCKH cấp trường', 8, N'II34B')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'NOIQUY', N'DAODUC', N'Không Vi Phạm Pháp Luật, Nội Quy Nhà Trường', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'RL1', N'RENLUYEN', N'Có > 65% sinh viên có kết quả đánh giá rèn luyện từ tốt trở lên và không có SV vi phạm kỹ luật từ cảnh cáo trở lên', 12, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'RL2', N'RENLUYEN', N'Có ít nhất 3 sinh viên tham gia hiến máu nhân đạo hoặc mỗi SV tham gia ít nhất 3 ngày tình nguyện trong năm học (có minh chứng/ có giấy xác nhận từ cấp Trường trở lên)', 6, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'RL3', N'RENLUYEN', N'Lớp tổ chức hoặc có tham gia ít nhất 1 phong trào văn nghệ, thể dục thể thao các cấp', 6, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'RL4', N'RENLUYEN', N'Tổ chức ít nhất 1 lần đến mái ấm nhà mở, tình nguyện vì cộng đồng', 5, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'RL5', N'RENLUYEN', N'Có > 90% sinh viên chấp hành tốt nội quy của Nhà trường (không xả rác, không hút thuốc lá, đeo bảng tên...(Căn cứ vào kết quả xét Hội đồng kỷ luật của Trường)', 6, N'LHTT')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'SVn1', N'HOINHAP', N'SV năm thứ 1 có điểm TB môn Ngoại ngữ năm học', 3, N'I1T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TLHT01', N'DAODUC', N'Có Tham Luận, Bài Viết hoăc Hội Thi Về Tư Tưởng Hồ Chí Minh từ cấp khoa', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TLHT02', N'DAODUC', N'Có Tham Luận, Bài Viết hoăc Hội Thi Về Tư Tưởng Hồ Chí Minh từ cấp Trường', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TNKHOE01', N'THELUC', N'Đạt Thanh Niên Khỏe từ cấp Khoa', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TNKHOE02', N'THELUC', N'Đạt Thanh Niên Khỏe từ cấp Trường', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TNTT', N'DAODUC', N'Đạt Thanh Niên Tiên Tiến Làm Theo Lời Bác', 0, N'III02T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TVDT', N'HOCTAP', N'Thành Viên Đội Tuyển Học Thuật Cấp Quốc Gia,Quốc Tế', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TVDT01', N'THELUC', N'Thành viên Đội tuyển cáp trường', 0, N'I2T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'TVDT02', N'THELUC', N'Thành viên Đội tuyển cáp Thành Phố, Quốc Gia', 0, N'II34T')
INSERT [dbo].[TieuChi] ([Ma_NDTieuChi], [MaTieuChi], [TenNDTieuChi], [Diem], [MaPhanCap]) VALUES (N'VHLS', N'UUTIEN', N'Tham Gia và Đạt Giải Khuyến Khích Trở Lên Trong Các Cuộc Thì về Tiềm Hiểu Văn Hóa, Lịch Sử, Xã Hội Trong Nước và Thế Giới Từ Cấp Trường', 0, N'II34T')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'DAODUC', N'Đạo Đức Tốt')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'HOCTAP', N'Học Tập Tốt')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'HOINHAP', N'Hội Nhập Tốt')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'KYNANG', N'Kỹ Năng')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'RENLUYEN', N'Rèn Luyện')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'THELUC', N'THể Lực Tốt')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'TINHNGUYEN', N'Tình Nguyện Tốt')
INSERT [dbo].[TieuChuan] ([MaTieuChi], [TenTieuChi]) VALUES (N'UUTIEN', N'Ưu Tiên')
INSERT [dbo].[TieuChuan_LHTT] ([MaCap], [DIem], [Ma_TC]) VALUES (N'K', 50, N'Kh')
INSERT [dbo].[TieuChuan_LHTT] ([MaCap], [DIem], [Ma_TC]) VALUES (N'TR', 66, N'Tr')
ALTER TABLE [dbo].[CanBo_HuongDan] ADD  DEFAULT ((0)) FOR [pass]
GO
ALTER TABLE [dbo].[PhieuDangKy_DeTai_NCKH] ADD  DEFAULT ('-1') FOR [TinhTrang]
GO
ALTER TABLE [dbo].[TaiKhoan_ADMIN] ADD  DEFAULT ('4') FOR [PhanCap]
GO
ALTER TABLE [dbo].[DeTai_NCKH]  WITH CHECK ADD FOREIGN KEY([MaPDK_NCKH])
REFERENCES [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH])
GO
ALTER TABLE [dbo].[DeTai_NCKH]  WITH CHECK ADD FOREIGN KEY([MSSV])
REFERENCES [dbo].[SinhVien] ([MSSV])
GO
ALTER TABLE [dbo].[KQ_danhgia_LHTT_SV]  WITH CHECK ADD FOREIGN KEY([Ma_NDTieuChi])
REFERENCES [dbo].[TieuChi] ([Ma_NDTieuChi])
GO
ALTER TABLE [dbo].[KQ_danhgia_LHTT_SV]  WITH CHECK ADD FOREIGN KEY([MaPDK_LHTT])
REFERENCES [dbo].[PhieuDangKy_LHTT] ([MaPDK_LHTT])
GO
ALTER TABLE [dbo].[KQ_danhgia_SV5T_SV]  WITH CHECK ADD FOREIGN KEY([Ma_NDTieuChi])
REFERENCES [dbo].[TieuChi] ([Ma_NDTieuChi])
GO
ALTER TABLE [dbo].[KQ_danhgia_SV5T_SV]  WITH CHECK ADD FOREIGN KEY([MaPDK_SV5T])
REFERENCES [dbo].[PhieuDangKy_SV5T] ([MaPDK_SV5T])
GO
ALTER TABLE [dbo].[KQ_DanhGiaND_LHTT_Admin]  WITH CHECK ADD FOREIGN KEY([TaiKhoan])
REFERENCES [dbo].[TaiKhoan_ADMIN] ([TaiKhoan])
GO
ALTER TABLE [dbo].[KQ_DanhGiaND_LHTT_Admin]  WITH CHECK ADD FOREIGN KEY([MaPDK_LHTT], [Ma_NDTieuChi])
REFERENCES [dbo].[KQ_danhgia_LHTT_SV] ([MaPDK_LHTT], [Ma_NDTieuChi])
GO
ALTER TABLE [dbo].[KQ_DanhGiaND_SV5T_Admin]  WITH CHECK ADD FOREIGN KEY([TaiKhoan])
REFERENCES [dbo].[TaiKhoan_ADMIN] ([TaiKhoan])
GO
ALTER TABLE [dbo].[KQ_DanhGiaND_SV5T_Admin]  WITH CHECK ADD FOREIGN KEY([Ma_NDTieuChi], [MaPDK_SV5T])
REFERENCES [dbo].[KQ_danhgia_SV5T_SV] ([Ma_NDTieuChi], [MaPDK_SV5T])
GO
ALTER TABLE [dbo].[KQ_DeTai_NCKH]  WITH CHECK ADD FOREIGN KEY([MaCap])
REFERENCES [dbo].[CapDanhHieu] ([MaCap])
GO
ALTER TABLE [dbo].[KQ_DeTai_NCKH]  WITH CHECK ADD FOREIGN KEY([MaPDK_NCKH])
REFERENCES [dbo].[PhieuDangKy_DeTai_NCKH] ([MaPDK_NCKH])
GO
ALTER TABLE [dbo].[Lop]  WITH CHECK ADD FOREIGN KEY([MaKhoa])
REFERENCES [dbo].[Khoa] ([MaKhoa])
GO
ALTER TABLE [dbo].[PDK_BaiBao_ThamLuan_NCKH]  WITH CHECK ADD FOREIGN KEY([MSSV])
REFERENCES [dbo].[SinhVien] ([MSSV])
GO
ALTER TABLE [dbo].[PhanCap_TieuChi]  WITH CHECK ADD FOREIGN KEY([PhanLoai])
REFERENCES [dbo].[PhanLoai] ([PhanLoai])
GO
ALTER TABLE [dbo].[PhieuDangKy_DeTai_NCKH]  WITH CHECK ADD FOREIGN KEY([Ma_GVHD2])
REFERENCES [dbo].[CanBo_HuongDan] ([MaGV])
GO
ALTER TABLE [dbo].[PhieuDangKy_DeTai_NCKH]  WITH CHECK ADD FOREIGN KEY([Ma_GVHD1])
REFERENCES [dbo].[CanBo_HuongDan] ([MaGV])
GO
ALTER TABLE [dbo].[PhieuDangKy_LHTT]  WITH CHECK ADD FOREIGN KEY([MaMinhChung])
REFERENCES [dbo].[MinhChung] ([MaMinhChung])
GO
ALTER TABLE [dbo].[PhieuDangKy_LHTT]  WITH CHECK ADD FOREIGN KEY([MSSV])
REFERENCES [dbo].[SinhVien] ([MSSV])
GO
ALTER TABLE [dbo].[PhieuDangKy_SV5T]  WITH CHECK ADD FOREIGN KEY([MaCap])
REFERENCES [dbo].[CapDanhHieu] ([MaCap])
GO
ALTER TABLE [dbo].[PhieuDangKy_SV5T]  WITH CHECK ADD FOREIGN KEY([MaMinhChung])
REFERENCES [dbo].[MinhChung] ([MaMinhChung])
GO
ALTER TABLE [dbo].[PhieuDangKy_SV5T]  WITH CHECK ADD FOREIGN KEY([MSSV])
REFERENCES [dbo].[SinhVien] ([MSSV])
GO
ALTER TABLE [dbo].[SinhVien]  WITH CHECK ADD FOREIGN KEY([MaLop])
REFERENCES [dbo].[Lop] ([MaLop])
GO
ALTER TABLE [dbo].[TieuChi]  WITH CHECK ADD FOREIGN KEY([MaPhanCap])
REFERENCES [dbo].[PhanCap_TieuChi] ([MaPhanCap])
GO
ALTER TABLE [dbo].[TieuChi]  WITH CHECK ADD FOREIGN KEY([MaTieuChi])
REFERENCES [dbo].[TieuChuan] ([MaTieuChi])
GO
ALTER TABLE [dbo].[TieuChuan_LHTT]  WITH CHECK ADD FOREIGN KEY([MaCap])
REFERENCES [dbo].[CapDanhHieu] ([MaCap])
GO
ALTER TABLE [dbo].[PhieuDangKy_DeTai_NCKH]  WITH CHECK ADD CHECK  (([TinhTrang]=(3) OR [TinhTrang]=(2) OR [TinhTrang]=(1) OR [TinhTrang]=(0) OR [TinhTrang]=(-1)))
GO
ALTER TABLE [dbo].[TaiKhoan_ADMIN]  WITH CHECK ADD CHECK  (([PhanCap]=(4) OR [PhanCap]=(3) OR [PhanCap]=(2) OR [PhanCap]=(1)))
GO
/****** Object:  StoredProcedure [dbo].[Add_KQ_DanhGia_LHTT_admin]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Add_KQ_DanhGia_LHTT_admin]
	@TaiKhoan nvarchar(20),
	@maND_TC varchar(10),
	@maPDK bigint
as
begin
	insert into KQ_DanhGiaND_LHTT_Admin values(@TaiKhoan,@maPDK,@maND_TC,0,null)
end
GO
/****** Object:  StoredProcedure [dbo].[Add_KQ_DanhGia_Sv5t_admin]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Add_KQ_DanhGia_Sv5t_admin]
	@TaiKhoan nvarchar(20),
	@maND_TC varchar(10),
	@maPDK bigint
as
begin
	insert into KQ_DanhGiaND_SV5T_Admin values(@TaiKhoan,@maND_TC,@maPDK,0,null)
end
GO
/****** Object:  StoredProcedure [dbo].[Add_MinhChung]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------Thêm minh chứng -------
create proc [dbo].[Add_MinhChung]
	@TenMinhChung nvarchar(20),
	@URL nvarchar(100)
as
	insert into MinhChung values (@TenMinhChung, @URL)


GO
/****** Object:  StoredProcedure [dbo].[Capnhat_TTSV]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Capnhat_TTSV]
@sdt bigint,
@quequan varchar(30),
@email varchar(50),
@noisinh varchar(20),
@diachi_cutru varchar(100),
@diachi_lienlac varchar(100),
@ngayvaodoan date,
@ngay_dang_dubi date,
@ngaydang_chinhthuc date,
@chucvu_doan varchar(30),
@mssv varchar(10)
as
begin
	update SinhVien set SDT = @sdt,QueQuan = @quequan,Email = @email, NoiSinh = @noisinh,
	DiaChiCuTru = @diachi_cutru, DiaChiLienLac = @diachi_cutru, NgayKetNapDoan = @ngayvaodoan,NgayVaoDang_dubi_neuco = @ngay_dang_dubi,
	NgayVaoDang_chinhthuc_neuco = @ngaydang_chinhthuc, ChucVuDoan_neuco = @chucvu_doan
	where MSSV = @mssv
	end


GO
/****** Object:  StoredProcedure [dbo].[Change_passSV]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Change_passSV]
	@MSSV char(10),
	@pass nvarchar(30),
	@pass_moi nvarchar(30)
as
begin
	update SinhVien
	set password = @pass_moi
	where @MSSV = MSSV and @pass = password
end



GO
/****** Object:  StoredProcedure [dbo].[Danh_Gia_TieuChi_ChuaDat]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Danh_Gia_TieuChi_ChuaDat]
@maPDK bigint,
@maND varchar(10)
as
begin
	update KQ_DanhGiaND_SV5T_Admin set Dat = 0
	where MaPDK_SV5T = @maPDK and KQ_DanhGiaND_SV5T_Admin.Ma_NDTieuChi = @maND
	end

GO
/****** Object:  StoredProcedure [dbo].[Danh_Gia_TieuChi_ChuaDat_lhtt]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	Create proc [dbo].[Danh_Gia_TieuChi_ChuaDat_lhtt]
	@maPDK bigint,
	@maND varchar(10)
	as
	begin

		update KQ_DanhGiaND_LHTT_Admin set Diem = 0
		where MaPDK_LHTT = @maPDK and KQ_DanhGiaND_LHTT_Admin.Ma_NDTieuChi = @maND
	end
GO
/****** Object:  StoredProcedure [dbo].[Danh_Gia_TieuChi_Dat]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Danh_Gia_TieuChi_Dat]
@maPDK bigint,
@maND varchar(10)
as
begin
	update KQ_DanhGiaND_SV5T_Admin set Dat = 1
	where MaPDK_SV5T = @maPDK and KQ_DanhGiaND_SV5T_Admin.Ma_NDTieuChi = @maND
	end

GO
/****** Object:  StoredProcedure [dbo].[Danh_Gia_TieuChi_Dat_lhtt]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Danh_Gia_TieuChi_Dat_lhtt]
@maPDK bigint,
@maND varchar(10)
as
begin

	update KQ_DanhGiaND_LHTT_Admin set Diem = (SELECT Diem FROM TieuChi WHERE Ma_NDTieuChi=@maND)
	where MaPDK_LHTT = @maPDK and KQ_DanhGiaND_LHTT_Admin.Ma_NDTieuChi = @maND
end
GO
/****** Object:  StoredProcedure [dbo].[Del_PDK_NCKH_theoMaPDK]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Del_PDK_NCKH_theoMaPDK]
	@maPDK_NCKH bigint
as
begin
	delete from DeTai_NCKH
	where MaPDK_NCKH = @maPDK_NCKH

	delete from PhieuDangKy_DeTai_NCKH
	where MaPDK_NCKH = @maPDK_NCKH
end

GO
/****** Object:  StoredProcedure [dbo].[DKy_DSachSV_NCKH]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DKy_DSachSV_NCKH]
	@MaPDK_NCKH Bigint,
	@MSSV_SV1 char(10),
	@MSSV_SV2 char(10),
	@MSSV_SV3 char(10)
as
begin
	insert into DeTai_NCKH values (@MaPDK_NCKH, @MSSV_SV1)
	insert into DeTai_NCKH values (@MaPDK_NCKH, @MSSV_SV2)
	insert into DeTai_NCKH values (@MaPDK_NCKH, @MSSV_SV3)
end



GO
/****** Object:  StoredProcedure [dbo].[DKy_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DKy_LHTT]
	@MaMinhChung bigint,
	@NamHoc int,
	@MSSV char(10)
as
	insert into PhieuDangKy_LHTT values (@MaMinhChung, @NamHoc, @MSSV)
	declare @maDK int
	set @maDK = SCOPE_IDENTITY()
	select @maDK AS MaDK


GO
/****** Object:  StoredProcedure [dbo].[DKy_LHTT_2]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DKy_LHTT_2]
	@MaPDK_LHTT int,
	@MaND varchar(10)
as
	insert into KQ_danhgia_LHTT_SV values (@MaPDK_LHTT,@MaND,0)
GO
/****** Object:  StoredProcedure [dbo].[DKy_NCKH]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[DKy_NCKH]
	@Ma_GVHD1 varchar(8),
	@Ma_GVHD2 varchar(8),
	@TenDeTai nvarchar(100),
	@NgayDK datetime,
	@ND_DeTai nvarchar(200)
as
begin
	insert into PhieuDangKy_DeTai_NCKH values (@Ma_GVHD1, @Ma_GVHD2, @TenDeTai, @NgayDK, @ND_DeTai, -1)
	declare @maPDK int
	set @maPDK = SCOPE_IDENTITY()
	select @maPDK AS MaPDK
end

GO
/****** Object:  StoredProcedure [dbo].[DKy_NCKH_2]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DKy_NCKH_2]
	@MSSV char(10),
	@MaPDK_NCKH Bigint
as
	insert into DeTai_NCKH values (@MSSV, @MaPDK_NCKH)




GO
/****** Object:  StoredProcedure [dbo].[DKy_SV5T]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DKy_SV5T]
	@MaMinhChung bigint,
	@MaCap varchar(10),
	@NamHoc int,
	@MSSV char(10)
as
	insert into PhieuDangKy_SV5T values (@MaMinhChung, @MaCap, @NamHoc, @MSSV)
	declare @maPDK int
	set @maPDK = SCOPE_IDENTITY()
	select @maPDK AS MaPDK

GO
/****** Object:  StoredProcedure [dbo].[DKy_SV5T_2]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DKy_SV5T_2]
	@MaPDK_SV5T Bigint,
	@Ma_ND varchar(10)
as
	insert into KQ_danhgia_SV5T_SV values (@Ma_ND, @MaPDK_SV5T,0)


GO
/****** Object:  StoredProcedure [dbo].[Ds_Dat_SV5T_Khoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Ds_Dat_SV5T_Khoa]
@makhoa varchar(10),
@soluong_tc int
as
select sv.Ho,sv.Ten, sv.MSSV,l.MaLop,dk.NamHoc, tong = count(case when kq.Dat = 1 then 1 else 0 end)
from PhieuDangKy_SV5T dk, SinhVien sv, Lop l, KQ_DanhGiaND_SV5T_Admin kq
where  kq.MaPDK_SV5T = dk.MaPDK_SV5T and dk.MSSV = sv.MSSV and sv.MaLop = l.MaLop and l.MaKhoa = @makhoa
group by sv.Ho,sv.Ten,sv.MSSV,l.MaLop,dk.NamHoc
having count(case when kq.Dat = 1 then 1 else null end) = @soluong_tc
GO
/****** Object:  StoredProcedure [dbo].[DS_DeTai_NCKH_theoGV]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[DS_DeTai_NCKH_theoGV]
	@maGV varchar(8)
as
begin
	select MaPDK_NCKH, TenDeTai, ND_DeTai, NgayDK, TinhTrang
	from PhieuDangKy_DeTai_NCKH a, CanBo_HuongDan b
	where (b.MaGV = a.Ma_GVHD1 or b.MaGV = a.Ma_GVHD2) and b.MaGV = @maGV
end

GO
/****** Object:  StoredProcedure [dbo].[DS_DeTai_NCKH_theoKhoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DS_DeTai_NCKH_theoKhoa]
	@maKhoa varchar(5)
as
begin
	select distinct Ma_GVHD1, Ma_GVHD2, d.MaPDK_NCKH, TenDeTai, ND_DeTai, NgayDK, TinhTrang, a.MSSV, Ho, Ten, b.MaLop
	from PhieuDangKy_DeTai_NCKH d, SinhVien a, Lop b, CanBo_HuongDan e
	where a.MaLop = b.MaLop and a.MSSV = d.ND_DeTai and (e.MaGV = d.Ma_GVHD1 or e.MaGV = d.Ma_GVHD2) and TinhTrang in (1,2,3) and ND_DeTai in 
	(
		select MSSV
		from SinhVien a, Lop b, Khoa c
		where a.MaLop = b.MaLop and b.MaKhoa = c.MaKhoa and c.MaKhoa = @maKhoa
	)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SV_NCKH_theoMaPDK]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[DS_SV_NCKH_theoMaPDK]
	@maPDK_NCKH bigint
as
begin
	select Ma_GVHD1, Ma_GVHD2, c.MaPDK_NCKH, TenDeTai, ND_DeTai, NgayDK, TinhTrang, c.MSSV, Ho, Ten, e.MaLop, TenKhoa
	from PhieuDangKy_DeTai_NCKH b, DeTai_NCKH c, SinhVien d, Lop e, Khoa f
	where b.MaPDK_NCKH = @maPDK_NCKH and b.MaPDK_NCKH = c.MaPDK_NCKH and d.MSSV = c.MSSV and d.MaLop = e.MaLop and e.MaKhoa = f.MaKhoa 
end

GO
/****** Object:  StoredProcedure [dbo].[DSach_DK_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DSach_DK_LHTT]
as
begin 
select PhieuDangKy_LHTT.MaPDK_LHTT,PhieuDangKy_LHTT.MSSV,SinhVien.Ho,SinhVien.Ten,PhieuDangKy_LHTT.NamHoc,SinhVien.Email
from PhieuDangKy_LHTT,SinhVien,Lop
where PhieuDangKy_LHTT.MSSV=SinhVien.MSSV and SinhVien.MaLop=Lop.MaLop
end
GO
/****** Object:  StoredProcedure [dbo].[DSach_DK_SV5T_Cap_TR/TH/TW]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DSach_DK_SV5T_Cap_TR/TH/TW]
as
begin
select distinct Lop.MaKhoa
from PhieuDangKy_SV5T,SinhVien, Lop
where PhieuDangKy_SV5T.MSSV = SinhVien.MSSV and SinhVien.MaLop = Lop.MaLop
end
GO
/****** Object:  StoredProcedure [dbo].[DSach_DK_SV5T_TheoKhoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DSach_DK_SV5T_TheoKhoa]
@maKhoa varchar(10)
as
begin
select distinct SinhVien.MaLop
from PhieuDangKy_SV5T,SinhVien, Lop
where PhieuDangKy_SV5T.MSSV = SinhVien.MSSV and SinhVien.MaLop = Lop.MaLop and Lop.MaKhoa = @maKhoa
end

GO
/****** Object:  StoredProcedure [dbo].[DSach_DK_SV5T_TheoKhoa_Cap]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DSach_DK_SV5T_TheoKhoa_Cap]
	@maKhoa varchar(10),
	@macap varchar(5),
	@pageindex int,
	@pagesize int
	as
	begin
SELECT *
FROM (
    SELECT sv.Ho,sv.Ten,sv.MSSV,dk.MaPDK_SV5T,dk.MaCap, ROW_NUMBER() OVER (ORDER BY dk.MaPDK_SV5T) AS RowNum
    from SinhVien sv, PhieuDangKy_SV5T dk , Khoa, Lop
	where  dk.MaCap = @macap and dk.MSSV = sv.MSSV and sv.MaLop = Lop.MaLop and Lop.MaKhoa = Khoa.MaKhoa and Khoa.MaKhoa = @maKhoa
) AS DS_SV_DK_SV5T
WHERE DS_SV_DK_SV5T.RowNum BETWEEN @pageindex AND @pagesize
end
GO
/****** Object:  StoredProcedure [dbo].[DSach_TC_Theo_MaPDK]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DSach_TC_Theo_MaPDK]
	@maPDk bigint
	as
	begin
select  kqsv.Ma_NDTieuChi,kq.Dat,kqsv.TinhTrang,tc.TenNDTieuChi,tc.Diem, kq.GhiChu
from KQ_DanhGiaND_SV5T_Admin kq,KQ_danhgia_SV5T_SV kqsv, TieuChi tc
where  kq.Ma_NDTieuChi = kqsv.Ma_NDTieuChi and kqsv.MaPDK_SV5T = kq.MaPDK_SV5T and kq.MaPDK_SV5T = @maPDk and kq.Ma_NDTieuChi = tc.Ma_NDTieuChi
end
GO
/****** Object:  StoredProcedure [dbo].[DSach_TC_Theo_MaPDK_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DSach_TC_Theo_MaPDK_LHTT]
	@maPDk bigint
	as
	begin
select  kqsv.Ma_NDTieuChi,kq.Diem,kqsv.TinhTrang,tc.TenNDTieuChi, kq.GhiChu, tc.Diem as[DiemTC]
from KQ_DanhGiaND_LHTT_Admin kq,KQ_danhgia_LHTT_SV kqsv, TieuChi tc
where  kq.Ma_NDTieuChi = kqsv.Ma_NDTieuChi and kqsv.MaPDK_LHTT = @maPDk and kq.MaPDK_LHTT=kqsv.MaPDK_LHTT and kq.Ma_NDTieuChi = tc.Ma_NDTieuChi
end
GO
/****** Object:  StoredProcedure [dbo].[DsachDK_SV5T_Lop]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  proc [dbo].[DsachDK_SV5T_Lop]
	@MaLop varchar(10),
	@MaCap varchar(10)
as
begin
	select distinct sv.Ho, sv.Ten,sv.MSSV,dk.MaCap,dk.MaPDK_SV5T
	from SinhVien sv, PhieuDangKy_SV5T dk , Khoa, Lop
	where  dk.MaCap = @MaCap and dk.MSSV = sv.MSSV and sv.MaLop = Lop.MaLop and Lop.MaLop = @MaLop 
end

GO
/****** Object:  StoredProcedure [dbo].[GetInfoSV]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Xem thông tin SV--
create proc [dbo].[GetInfoSV] 
	@MSSV char(10)
as
begin
	select *
	from SinhVien a
	where a.MSSV = @MSSV
end



GO
/****** Object:  StoredProcedure [dbo].[GetListSV5T_theokhoa_theolopdautiencoSV5T]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetListSV5T_theokhoa_theolopdautiencoSV5T]
	@MaKhoa varchar(5)
as
begin
	select *
	from SinhVien a, PhieuDangKy_SV5T b
	where a.MSSV = b.MSSV and a.MaLop in
	(
		select top 1 a.MaLop
		from Lop a, SinhVien b, PhieuDangKy_SV5T c
		where b.MSSV = c.MSSV and a.MaLop = b.MaLop and MaKhoa = @MaKhoa
	)
end

GO
/****** Object:  StoredProcedure [dbo].[GetTaiLieu_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------Xem Tài liệu tiêu chí LHTT -----------
create proc [dbo].[GetTaiLieu_LHTT]
as
begin
	select *
	from TieuChi a
	where MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi
		where MaPhanCap = 'LHTT'
	)
end



GO
/****** Object:  StoredProcedure [dbo].[GetTaiLieu_SV5T_TheoCap]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------Xem Tài liệu tiêu chí SV5T theo cấp -----------
create proc [dbo].[GetTaiLieu_SV5T_TheoCap]
	@MaCap varchar(10)
as
begin
if(@Macap = 'K')
(	
	select *
	from TieuChi a
	where MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi
		where Khoa=1 and MaPhanCap <> 'LHTT'
	)
)
if(@MaCap = 'TR')
(
	select *
	from TieuChi a
	where MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi
		where Truong=1 and MaPhanCap <> 'LHTT'
	)
)
if(@MaCap = 'TH' or @MaCap = 'TW')
(
	select *
	from TieuChi a
	where MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi
		where (Thanh=1 or TrungUong=1) and MaPhanCap <> 'LHTT'
	)
)
end



GO
/****** Object:  StoredProcedure [dbo].[GetTieuChiLHTT_TheoPDK]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetTieuChiLHTT_TheoPDK]
	@maPDK bigint
as
	select *
	from PhieuDangKy_LHTT a, TieuChi b, KQ_danhgia_LHTT_SV c
	where c.MaPDK_LHTT = @maPDK and  a.MaPDK_LHTT = c.MaPDK_LHTT and  c.Ma_NDTieuChi = b.Ma_NDTieuChi and b.MaPhanCap = 'LHTT'
GO
/****** Object:  StoredProcedure [dbo].[GetTieuChiSV5T_TheoMSSV_TheoCap]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetTieuChiSV5T_TheoMSSV_TheoCap]
	@MSSV nvarchar(10),
	@MaCap varchar(10)
as
begin
if(@MaCap = 'K')(
	select *
	from PhieuDangKy_SV5T a, TieuChi b, KQ_danhgia_SV5T_SV c
	where a.MaPDK_SV5T = c.MaPDK_SV5T and a.MSSV = @MSSV and c.Ma_NDTieuChi = b.Ma_NDTieuChi and b.MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi a
		where Khoa = '1' and MaPhanCap <> 'LHTT'
	)
)
if(@MaCap = 'TR')(
	select *
	from PhieuDangKy_SV5T a, TieuChi b, KQ_danhgia_SV5T_SV c
	where a.MaPDK_SV5T = c.MaPDK_SV5T and a.MSSV = @MSSV and c.Ma_NDTieuChi = b.Ma_NDTieuChi and b.MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi a
		where Truong = '1' and MaPhanCap <> 'LHTT'
	)
)
if(@MaCap = 'TH' or @MaCap = 'TW')(
	select *
	from PhieuDangKy_SV5T a, TieuChi b, KQ_danhgia_SV5T_SV c
	where a.MaPDK_SV5T = c.MaPDK_SV5T and a.MSSV = @MSSV and c.Ma_NDTieuChi = b.Ma_NDTieuChi and b.MaPhanCap in
	(
		select MaPhanCap
		from PhanCap_TieuChi a
		where Thanh = '1' and MaPhanCap <> 'LHTT'
	)
)
end

GO
/****** Object:  StoredProcedure [dbo].[GhiChu_MinhChung]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GhiChu_MinhChung]
	@ghichu nvarchar(200),
	@maPDK bigint,
	@maTC varchar(10)
as
begin
	update KQ_DanhGiaND_SV5T_Admin
	set GhiChu = @ghichu
	where MaPDK_SV5T = @maPDK and Ma_NDTieuChi = @maTC
end
GO
/****** Object:  StoredProcedure [dbo].[GhiChu_MinhChung_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GhiChu_MinhChung_LHTT]
	@ghichu nvarchar(200),
	@maPDK bigint,
	@maTC varchar(10)
as
begin
	update KQ_DanhGiaND_LHTT_Admin
	set GhiChu = @ghichu
	where MaPDK_LHTT = @maPDK and Ma_NDTieuChi = @maTC
end
GO
/****** Object:  StoredProcedure [dbo].[Lay_TK_Ad]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Lay_TK_Ad]
	@ma nvarchar(20)
as
begin
	select *
	from TaiKhoan_ADMIN
	where TaiKhoan like @ma+'%'
	order by TaiKhoan DESC
end
GO
/****** Object:  StoredProcedure [dbo].[Login_Acc]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Login_Acc]
	@MSSV nvarchar(10),
	@pass nvarchar(30)
as
begin
	select MSSV
	from SinhVien
	where MSSV = @MSSV and password = @pass
end



GO
/****** Object:  StoredProcedure [dbo].[PDK_NCKH_GV_duyet]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[PDK_NCKH_GV_duyet]
	@maPDK_NCKH bigint
as
begin
	UPdate PhieuDangKy_DeTai_NCKH
	SET TinhTrang = 1	
	where MaPDK_NCKH = @maPDK_NCKH
end

GO
/****** Object:  StoredProcedure [dbo].[PDK_NCKH_GV_huy]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[PDK_NCKH_GV_huy]
	@maPDK_NCKH bigint
as
begin
	UPdate PhieuDangKy_DeTai_NCKH
	SET TinhTrang = 0	
	where MaPDK_NCKH = @maPDK_NCKH
end

GO
/****** Object:  StoredProcedure [dbo].[PDK_NCKH_GV_Sua]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[PDK_NCKH_GV_Sua]
	@maPDK_NCKH bigint,
	@ten_dt nvarchar(100)
as
begin
	UPdate PhieuDangKy_DeTai_NCKH
	SET TenDeTai = @ten_dt	
	where MaPDK_NCKH = @maPDK_NCKH
end
GO
/****** Object:  StoredProcedure [dbo].[PDK_NCKH_Khoa_duyet]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[PDK_NCKH_Khoa_duyet]
	@maPDK_NCKH bigint
as
begin
	UPdate PhieuDangKy_DeTai_NCKH
	SET TinhTrang = 2	
	where MaPDK_NCKH = @maPDK_NCKH
end
GO
/****** Object:  StoredProcedure [dbo].[QuanLyLopTruong_theoKhoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[QuanLyLopTruong_theoKhoa]
	@mssv char(10),
	@tinhtrang bit
as
begin
	update SinhVien
	set loptruong = @tinhtrang
	where MSSV = @mssv
end
GO
/****** Object:  StoredProcedure [dbo].[Saukhi_upminhchung]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Saukhi_upminhchung]
	@MaPDK varchar(20),
	@Ma_NDTC varchar(10)
as
begin
	UPdate KQ_danhgia_SV5T_SV
	SET TinhTrang = 1
	where MaPDK_SV5T = @MaPDK and Ma_NDTieuChi = @Ma_NDTC
end


GO
/****** Object:  StoredProcedure [dbo].[Saukhi_upminhchung_LHTT]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Saukhi_upminhchung_LHTT]
	@MaPDK varchar(20),
	@Ma_NDTC varchar(10)
as
begin
	UPdate KQ_danhgia_LHTT_SV
	SET TinhTrang = 1
	where MaPDK_LHTT = @MaPDK and Ma_NDTieuChi = @Ma_NDTC
end
GO
/****** Object:  StoredProcedure [dbo].[SL_DK_LHTT_TheoKhoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SL_DK_LHTT_TheoKhoa]
@maKhoa varchar(10)
as
begin
select tong= COUNT(MaPDK_LHTT)
from PhieuDangKy_LHTT dt, SinhVien sv, Lop l
where dt.MSSV = sv.MSSV AND sv.MaLop = l.MaLop and l.MaKhoa = @maKhoa
end

GO
/****** Object:  StoredProcedure [dbo].[SL_DK_NCKH_TheoKhoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[SL_DK_NCKH_TheoKhoa]
@maKhoa varchar(10)
as
begin
	select tong= COUNT(MaPDK_NCKH)
	from PhieuDangKy_DeTai_NCKH dt
	where TinhTrang in (1,2,3) and ND_DeTai in 
	(
		select MSSV
		from SinhVien a, Lop b, Khoa c
		where a.MaLop = b.MaLop and b.MaKhoa = c.MaKhoa and c.MaKhoa = @maKhoa
	)
end

GO
/****** Object:  StoredProcedure [dbo].[SoLuong_DK_SV5T_TheoKhoa]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SoLuong_DK_SV5T_TheoKhoa]
@macap varchar(10),
@makhoa varchar(10)
as
begin
select Tong = count(dk.MaCap)
from PhieuDangKy_SV5T dk, SinhVien sv, Lop l
where dk.MaCap = @macap and dk.MSSV = sv.MSSV and sv.MaLop = l.MaLop and l.MaKhoa = @makhoa
end

GO
/****** Object:  StoredProcedure [dbo].[Sua_MatKhau_Admin]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sua_MatKhau_Admin]
	@tk varchar(20),
	@pass nvarchar(20)
as
begin
	update TaiKhoan_ADMIN
	set MatKhau = @pass
	where TaiKhoan = @tk
end
GO
/****** Object:  StoredProcedure [dbo].[Them_TaiKhoan_Admin]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Them_TaiKhoan_Admin]
	@taikhoan nvarchar(20),
	@pass nvarchar(20),
	@ma_pc int
as
begin
	insert into TaiKhoan_ADMIN values (@taikhoan,@pass,@ma_pc)
end
GO
/****** Object:  StoredProcedure [dbo].[ThoiGian_DangKy_PhongTrao]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ThoiGian_DangKy_PhongTrao]
	@maHD char(10),
	@ngaybd Date,
	@ngaykt Date
as
begin
	update ThoiGianHD
	set TG_BatDau = @ngaybd,  TG_KetThuc = @ngaykt
	where TenHD = @maHD
end
GO
/****** Object:  StoredProcedure [dbo].[ThongKe_DK_SV5T_TheoCap]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ThongKe_DK_SV5T_TheoCap]
	@macap varchar(10)
as
begin
SELECT  case count(MaCap) when 0 then 0 else count(MaCap) end as SL
FROM PhieuDangKy_SV5T dk
where MaCap = @macap
end


GO
/****** Object:  StoredProcedure [dbo].[Xoa_TaiKhoan_Admin]    Script Date: 29/12/2018 03:00:34 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Xoa_TaiKhoan_Admin]
	@taikhoan nvarchar(20)
as
begin
	delete TaiKhoan_ADMIN
	where TaiKhoan = @taikhoan
end
GO
USE [master]
GO
ALTER DATABASE [DACN] SET  READ_WRITE 
GO
